import React from 'react'; 
import {Route, Switch} from 'react-router-dom';
import './LayoutBasic.scss';
import MenuTop from '../../Components/Menu/TopMenu';
import Footer from '../../Components/Footer';
import TopMenuMobile from '../../Components/Menu/TopMenuMobile';



const LayoutBasic = ({routes}) => {
    const w = window.innerWidth;
    
    return (  
        <div className="layout-basic">
        <div className="layout-basic__menu-top">
        {w<1024
        ?<TopMenuMobile /> :<MenuTop/>}
        </div>
        <LoadRouters 
            routes = {routes}/>
        <Footer/>
        </div>
            
    );
}
 
export default LayoutBasic;



function LoadRouters({routes}){
  
    return (
        
        <Switch>
            {routes.map((route, index) =>(
              <Route 
                  key= {index}
                  path = {route.path}
                  exact = {route.exact}
                  component = {route.component}//no se van a renderizar mas componentes mediante rutas
              />
          ))}
        </Switch>
    );
  }