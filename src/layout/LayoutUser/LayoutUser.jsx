import React, {useState, useEffect} from 'react';
import 'antd/dist/antd.css';
import './LayoutUser.scss';
import {Route, Switch} from 'react-router-dom';
import {Layout} from 'antd';
import MenuTop from '../../Components/User/MenuTop';
import MenuSider from '../../Components/User/MenuSider';
import firebase  from '../../config/FireBase';
import'firebase/auth';
import'firebase/firestore';
import UseAuth from '../../Hooks/UseAuth';




const LayoutAdmin = (props) => {
    //Sacar rutas de los props
    const {routes} = props;
    const {Header, Content, Footer} = Layout;//Ant design
    const [menuColapsed, setmenuColapsed] = useState(true);
    const [userLogged, setUserLogged] = useState(false);
    const [loading, setloading] = useState(true);
    const {authState, setAuthState} = UseAuth(); 
  
    useEffect(() => {

        firebase.auth().onAuthStateChanged(function(user) {
           
        });
        firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            const uid = firebase.auth().currentUser.uid;
            firebase.firestore().collection("usuarios").doc(uid).get().then(function(doc) {
                if (doc.exists) {   
                    setAuthState({id:uid, ...doc.data()})
                    if(doc.data().status===true){
                        setUserLogged(true);
                        setloading(false);
                    }else{
                        window.location.href = '/login';
                    }
                } else {
                    window.location.href = '/login';
                    setUserLogged(false);
                    setloading(false); 
                }
            })
        } else {
           window.location.href = '/login';
            setUserLogged(false);
            setloading(false);
        }
            });
  
    }, [])
    
    if (!userLogged&&!loading){
        window.location.href ='/login';
    }
    if(userLogged&&!loading){
           
        return (  
            <Layout>            
             <MenuSider menuColapsed = {menuColapsed}/>
        
                    <Layout className = "layout-admin" >
                        
                        <div>
                        <Header className ="layout-admin__header">
                          <MenuTop setmenuColapsed = {setmenuColapsed} menuColapsed ={menuColapsed} />
                        </Header>
                        </div>
                        <div>
                        <Content className="layout-admin__content"style = {{marginLeft: menuColapsed ?"80px" :"200px"}}>
                           <LoadRouters routes = {routes}/> 
                        </Content>
                        </div>
                        <Footer className="layout-admin__footer"  >Derechos reservados para <strong>COVITRIP21</strong></Footer>
                    </Layout>
                 </Layout>
            );      
    }return null
}
    


function LoadRouters({routes}){
    
    return (
        <Switch>
            {routes.map((route, index) =>(
            <Route 
                key= {index}
                path = {route.path}
                exact = {route.exact}
                component = {route.component}//no se van a renderizar mas componentes mediante rutas
            />
            ))}
        </Switch>
    );
}
 
export default LayoutAdmin;