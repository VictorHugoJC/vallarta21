import React, {createContext, useState} from 'react';


export  const QuotationContext = createContext();

export default function QuotationProvider (props) {
   
    const {children} = props;
    const [quotationState, setQuotationState] = useState({});

    
    return (
        <QuotationContext.Provider
            value={{quotationState, setQuotationState}}
        > {children}</QuotationContext.Provider>
        );
    }
