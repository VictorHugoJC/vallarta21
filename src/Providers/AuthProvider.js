import React, {createContext, useState} from 'react';
import firebase from '../config/FireBase';
import 'firebase/auth';
import 'firebase/firestore';
import {notification} from 'antd'
 
export  const AuthContext = createContext();

export default function AuthProvider (props) {
   
    const {children} = props;
    const [authState, setAuthState] = useState({});

    const auth=()=>{
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                const uid = firebase.auth().currentUser.uid;
                firebase.firestore().collection("usuarios").doc(uid).get().then(function(doc) {
                    if (doc.exists) {   
                       
                        if(doc.data().role==='user'){
                                    notification["success"]({
                                        message: `Bienvenido ${doc.data().name}`
                                    })
                                    window.location.href='/user';
                        }else{
                                    window.location.href= '/login';
                                    notification["error"]({
                                        message: "error de autentificación"
                                    })
                            }
                        }else{
                            firebase.firestore().collection("administradores").doc(uid).get().then(function(doc) {
                                if (doc.exists) {   
                                    
                                   
                                    if(doc.data().role==='admin'||doc.data().role==='superAdmin'){
                                                notification["success"]({
                                                    message: `Bienvenido ${doc.data().name}`
                                                })
                                                window.location.href='/admin';
                                    }else{
                                                notification["error"]({
                                                    message: "error en la conexión con el servidor"
                                                })
                                                firebase.auth().signOut(); 
                                                
                                        }
                                }else{
                                    firebase.auth().signOut();
                                }
                            }).catch(()=>{
                                firebase.auth().signOut();
                            })
                        }
                }).catch(()=>{
                    
                    firebase.auth().signOut();
                })
            }
        })
    }   

   
    return (
        <AuthContext.Provider
            value={{authState, 
                    setAuthState, auth}}
        > {children}</AuthContext.Provider>
        );
    }