//basicLayout
import LayoutBasic from '../layout/LoyoutBasic';

//Admin layout
import LayoutAdmin from '../layout/LayoutAdmin';

//user layout
import LayoutUser from '../layout/LayoutUser';
//pages
import Home from '../pages/Home';
import Process from '../pages/Process';
import VacationPlan from '../pages/VacationPlan';
import Login from '../pages/Login'
import Error404 from "../pages/Error404";
import Policy from '../pages/Policy';
import Register from '../pages/Register';
import QuotationPage from '../pages/QuotationPage';


//admin pages
import Users from '../pages/Admin/Users';
import Payments from '../pages/Admin/Payments';
import AdminUsers from '../pages/Admin/AdminUsers';
import AdminHomeHome from '../pages/Admin/AdminHome'; 
import Cupons from '../pages/Admin/Cupons';
import Rooms from '../pages/Admin/Rooms';
import Configuration from '../pages/Admin/Configuration';

//user pages
import User from '../pages/User';
import Info from '../pages/User/Info';
import PaymentsUser from '../pages/User/PaymentsUser';


const routes =[
    {
        path: "/user",
        component: LayoutUser,
        exact: false,
        routes:[
            {
                path: "/user",
                component: User,
                exact: true
            },
            {
                path: "/user/info",
                component: Info,
                exact: true
            },
            {
                path: "/user/recibos",
                component: PaymentsUser,
                exact: true
            }
        ]
    },
    
    {
        path: "/admin",//Basic
        component: LayoutAdmin,
        exact: false ,
        routes: [
            {
                path: "/admin",
                component: AdminHomeHome,
                exact: true
            },{
                path: "/admin/users",
                component: Users,
                exact: true
            },
            {
                path: "/admin/admin-profiles",
                component: AdminUsers,
                exact: true
            },
            {
                path: "/admin/payments",
                component: Payments,
                exact: true
            },
            {
                path: "/admin/cupons",
                component: Cupons,
                exact: true
            },
            {
                path: "/admin/rooms",
                component: Rooms,
                exact: true
            },{
                path: "/admin/configuration",
                component: Configuration,
                exact: true
            },
            {component: Error404}
        ]
    },
   
    {
        path: "/",//Basic
      component: LayoutBasic,
      exact: false,
      routes: [
          {
          path: "/",
          component: Home,
          exact: true
      },
      {
          path: "/process",
          component: Process,
          exact: true
      },
      {
          path: "/vacation-plan",
          component: VacationPlan,
          exact: true
      },
      
      {
          path: "/terms-of-service",
          component: Policy,
          exact: true
      },{
          path: "/register",
          component: Register,
          exact: true
      },
      {     path: "/login",
            component: Login,
            exact: true},
      {
          path: "/cotizacion",
          component: QuotationPage,
          exact: true
      },
      {component: Error404}
  ]
  },
]

export default routes;