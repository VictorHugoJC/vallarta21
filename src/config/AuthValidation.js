import React,{useEffect, useState} from 'react';
import UseAuth from '../Hooks/UseAuth';
import firebase from '../config/FireBase';
import 'firebase/auth';
import 'firebase/firestore';
import {notification} from 'antd';

export function AuthValidation(){
    const {setAuthState, authState} =UseAuth();
    const [userData, setuserData] = useState(null);
    useEffect(() => {
        

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                const uid = firebase.auth().currentUser.uid;
                const email = firebase.auth().currentUser.email;

                
                firebase.firestore().collection("usuarios").doc(uid).get().then(function(doc) {
                    if (doc.exists) {
                        
                        if(doc.data()){
                            setuserData(doc.data());
                            setuserData({...userData, email: email, uid: uid});
                            
                        }else{
                            firebase.auth().signOut();
                        }
                        
                    } else {
                        firebase.auth().signOut();
                       notification['error']({
                           message:"Error al conectarse a la base de datos"
                       })
                    }
                }).catch(function(error) {
                    firebase.auth().signOut();
                });
            
            } else {
                
            }
            });
            
    }, [])

    if(userData){
        setAuthState(userData);
        return true
    }else{
        return false
    }


    

}
 

