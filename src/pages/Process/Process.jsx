import React,{Fragment, useEffect} from 'react'
import * as Scroll from 'react-scroll';
import {Col,Row, Card,Divider, Button} from 'antd';
import './Process.scss';
import {Link} from 'react-router-dom';
import {DollarCircleOutlined,CreditCardOutlined,
    ScheduleOutlined,HeartOutlined} from '@ant-design/icons';
import Helmet from 'react-helmet';

    import Quotation from '../../Components/Forms/Quotation';


const Process = () => {
    useEffect(() => {
        Scroll.animateScroll.scrollToTop();
    }, []);

    return (  
        <Fragment>
        <Helmet>
            <title>Proceso</title>
        </Helmet>
        <div className="main-banner"/>

        <div className="content-process">
        <Row>
                <Col xs={4}/>
                <Col xs={16}>
                    <h1>
                       PROCESO DE PAGOS
                    </h1>
                    <Card className="content-process-card">
                        <Card.Grid  className="content-process-card-grid">
                            <CardProcess  title={"Cotiza"} icon={<DollarCircleOutlined />} link={"Cotiza"} key={0}/>
                            <CardProcess title={"Aparta"} icon={<CreditCardOutlined/>} link={"Aparta"} key={1}/>
                            <CardProcess title={"Planifica"} icon ={<ScheduleOutlined />} link={"Planifica"} key={2}/>
                            <CardProcess title={"Disfruta"} icon={<HeartOutlined />} link={"Disfruta"} key={3}/>
                       </Card.Grid>
                        
                    </Card>
                </Col>
                <Col xs={4}/>
        </Row>
        </div>
        <div className="content-process-descriotions">
    <ProcessDescription title={"Cotiza"} icon={<DollarCircleOutlined className="process-item"/>} 
           component={true} key={0}/>  
        <ProcessDescription title={"Aparta"} icon={<CreditCardOutlined className="process-item" />}  
            description={"Una vez realizada tu cotización, procede a generar el pago de tu anticipo con 10% para poder apartar tu lugar en esta increíble aventura, el costo delanticipo es deacuerdo al total de la cotización de tu paquete."} key={1}/>  
        <ProcessDescription title={"Planifica"} icon ={<ScheduleOutlined className="process-item" />}
            description={"Por medio de nuestra plataforma enterate de datos sobre el viaje, tu progreso de abonos y ve pagando poco a poco. ¿Ya tienes tu cuenta?"} key={2}
            button={"iniciar Sesión"} link={"/login"}/>  
        <ProcessDescription title={"Disfruta"} icon={<HeartOutlined className="process-item" />} 
            description={"Ya que has cubierto el costo total del paquete, solo queda una última cosa, Disfrutar este grandioso viaje"} key={3}/>  
        </div>
       
        </Fragment>
    );
}
 
export default Process;

function CardProcess ({title,icon, link}) {
    return(
        <Fragment>
        <Scroll.Link to={link} smooth={true} offset={50} duration={800} delay={200}>
        <Card.Grid hoverable={true} className="content-process-card-grid-item">
            {icon} <br/>
            {title}
            </Card.Grid>
        </Scroll.Link>
        </Fragment>
    );
}

function ProcessDescription ({title, icon, description, button, link, component}){
    return(
    <Fragment>
    <div className={`process-description ${title}`}>
        <br/><br/>
        <Divider>{icon}<br/><h3>{title}</h3></Divider>
        {component ?<Quotation/> :
            <Row>
                <Col md={4}/>
                <Col md={16}>
                    <h4>{description}</h4>
                    
                    
                    <br/>
                    {button ?<Link to={link}><Button className="process-description__btn" type="primary">{button}</Button> </Link> :null}   
                </Col>
                <Col md={4}/>
            </Row>
        }
        
        </div>
    </Fragment>
    );
}