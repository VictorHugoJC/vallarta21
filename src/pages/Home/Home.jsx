import React,{Fragment, useEffect} from 'react'
import * as Scroll from 'react-scroll';
import {Row,Col,Button,Card} from 'antd';
import Helmet from 'react-helmet';


import './Home.scss';


import imgDestination from '../../assets/img/pv-images.png';
import MainBannerImg from '../../Components/MainBannerImg';

const Home = () => {
    useEffect(() => {
        Scroll.animateScroll.scrollToTop();
    }, []);

    return (  
        <Fragment>
            <Helmet>
                <title>
                    Un viaje inolvidable - aparta ya!
                </title>
            </Helmet>
            <MainBannerImg 
                title="CREAREMOS UN VIAJE INOLVIDABLE " 
                content=" Lo mejor esta por venir"
                />
            
        <div className="home-content"> 
           <div className="home-content-destination">
           <Row>
                <Col lg={4}/>
                <Col lg={16}>
                    <p><ion-icon name="location-sharp"></ion-icon> Destino:</p>
                    <h3>PUERTO VALLARTA, JALISCO</h3>
                    <img alt="" src={imgDestination} className="home-content-destination-img"/>
                </Col>
                <Col lg={4}/>
            </Row>
           </div>
           <div className="home-content-details">
                <h2>DETALLES DEL VIAJE</h2>
                <Row>
                <Col xs={4}/>
                <Col xs={16}>
                <CardDetail title={"SALIDA"} icon={<ion-icon name="location-sharp"></ion-icon>} 
                    description={`25 DE AGOSTO
                    2O21
                    San Luis Potosí, S.L.P.
                    `} key={0}/>
                <CardDetail title={"REGRESO"} icon={<ion-icon name="home-sharp"></ion-icon>} 
                    description={`29 DE AGOSTO
                    2O21
                    Puerto Vallarta, Jal.`}key={1}/>
                <CardDetail title={"INFORMES"} icon={<ion-icon name="help-circle-sharp"></ion-icon>} 
                    description={`PARA MÁS INFORMACIÓN
                    CONSULTE LA SECCIÓN
                    DE PAQUETE`}key={2}/>
                </Col>
                <Col xs={4}/>
                </Row>
           </div>
           <div className="home-content-promotion">
                <h1>¡PROMOCIÓN!</h1>
                <h3>Aparta tu Lugar a Tiempo y llevate Increíbles Descuentos</h3>
                <Row>
                <Col xs={4}/>
                <Col xs={16}>
                <CardProm title={"PRIMER PREVENTA"} description={"HASTA EL 19 DE DICIEMBRE DE 2020"}/>
                <CardProm title={"SEGUNDA PREVENTA"} description={"HASTA EL 18 DE FEBRERO DE 2021"}/>
                <CardProm title={"ULTIMA PREVENTA"} description={"HASTA EL 28 DE MARZO 2021"}/>
                </Col>
                <Col xs={4}/>
                </Row>
                <Button type="primary" className="home-content-promotion-btn">Cotizar</Button>
           </div>
        </div>
        </Fragment>
    );
}
 
export default Home;

function CardDetail ({title,icon, description}) {
    return(
       
        <Card.Grid hoverable={false} className="content-process-card-grid-item">
            {icon} <br/>
            <h2>{title}</h2>
            <p >{description}</p>
        </Card.Grid>
            
    );
}

function CardProm ({title, description}){
    return(
    <Card.Grid hoverable={false} className={`content-process-card-grid-item   ${title}`}>    
        <h2>{title}</h2>
        <ion-icon name="calendar-sharp"></ion-icon>
        <p >{description}</p>
    </Card.Grid>
        );

}
