import React, {useState} from 'react';

import { useEffect } from 'react';

import ListUser from '../../../Components/Admin/listUser';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';

import Helmet from 'react-helmet';


const Users = () => {
    const [usersActive, setUsersActive] = useState([]);
    const [usersInactive, setUsersInactive] = useState([]);
    const [reloadUsers, setReloadUsers] = useState(false);    

    useEffect(() => {

        setReloadUsers(false);
        firebase.firestore().collection('usuarios').where('status','==', true).onSnapshot(function(snapshot){
            const ActiveUsers = snapshot.docs.map( doc =>{
                return {
                    id: doc.id,
                    ...doc.data()
                }
            });
            setUsersActive(ActiveUsers);
           
        });
        firebase.firestore().collection('usuarios').where('status','==', false).onSnapshot(function(snapshot){
            const inactiveUsers = snapshot.docs.map( doc =>{
                return {
                    id: doc.id,
                    ...doc.data()
                }
            });
            setUsersInactive(inactiveUsers);
           
        });

    }, [ reloadUsers])

    return (  
        <>
        <Helmet>
            <title>Administración de usuarios</title>
        </Helmet>
        <div className="users">
            <ListUser useModal={true} usersActive = {usersActive} usersInactive = {usersInactive} setReloadUsers = {setReloadUsers}/>
        </div>
        </>
    );
}
 
export default Users;