import React, {useEffect, useState,} from 'react'
import UseAuth from '../../Hooks/UseAuth';
import {Row, Col} from 'antd';
import RegisterPayment from '../../Components/Forms/RegisterPayment';
import RegisterCuponCode from '../../Components/Forms/RegisterCuponCode';
import {Card, Progress } from 'antd';
import './Admin.scss';
import firebase from '../../config/FireBase';
import 'firebase/firestore';
import moment from 'moment';

import Helmet from 'react-helmet';


const AdminHome = () => {
    const {authState} = UseAuth();
    const [configData, setConfigData] = useState();
    const [stadistics, setStadistics] = useState({daysTo:0});
    const [amount, setamount] = useState(0);
    const [paid, setpaid] = useState(0);
    const [userCont, setuserCont] = useState(0);
    const [update, setupdate] = useState(false);



    useEffect(()=>{
        
        //obteber cuanto es el total
        /* setamount(doc.data().price+amount),
                    setuserCont(userCont+1) */
        
        
        firebase.firestore().collection('usuarios').where('status','==', true).onSnapshot(function(snapshot){
            const getUsers = snapshot.docs.map( doc =>{
               
                return ({
                    id: doc.id,
                    ...doc.data()

            })       
        })
        
        getamountfromusers(getUsers);
        
    })
        //obtener cuanto han pagado
        firebase.firestore().collection('pagos').where('amount','>', 0).onSnapshot(function(snapshot){
            const getAmount = snapshot.docs.map( doc =>{
               
                return ({
                    id: doc.id,
                    ...doc.data()
            });
        });
        getamountpaid(getAmount);


        
    });

        firebase.firestore().collection('config').doc('ZJJa8vl6cdhyUiGQwROt').get().then(function(doc){
            if(doc.exists){
                setConfigData(doc.data());
                
                const a = moment([doc.data().tripDate[0],doc.data().tripDate[1]-1,doc.data().tripDate[2]]);
                const b = moment();
                setStadistics({
                    ...stadistics,
                    daysTo: a.diff(b,'days'),
                    daysPercent: (a.diff(b,'days')*100)/300,
                });
            }
        });


        setupdate(false);
        // eslint-disable-next-line 
    },[update]);

    const getamountfromusers = (user) =>{
        let total=0;
        let uscont=0;
        setamount(0);
        setuserCont(0);
        // eslint-disable-next-line
        user.map(us=>{ 

            total+=us.price;
            uscont+=1;
        })
        setamount(total);
        setuserCont(uscont);
    }

    const getamountpaid = (paid) =>{
     
        let total=0;
        // eslint-disable-next-line 
        paid.map(us=>{ 

            total+=us.amount;
        })
        setpaid(total);
    }
    
    
    return (
        <>
            <Helmet>
                <title>Mi cuenta de administrador</title>
            </Helmet>
            {authState ?<h1>Hola <strong>{authState.name}</strong></h1> :null}
            <Row>
                <Col lg={2}/>
                <Col lg={20}>
                <Card>
                    <Card.Grid style={{width:"100%"}}>
                        <h2>Estadísticas</h2>
                        <Progress type="circle" percent={stadistics.daysPercent} format={percent => `${stadistics.daysTo} Días restantes`} status={stadistics.daysPercent<40 ?"exception" :null}/>
                        <Progress type="circle" percent={Math.round((paid*100)/amount)} format={percent => `${percent}% Pagado`} status={Math.round((paid*100)/amount)<40 ?"exception" :null}/>
                        <Progress type="circle" percent= {configData ? Math.round((userCont*100)/configData.maxUsers) : 0} format={percent => `${percent}% ocupado`}  status={configData ? Math.round((userCont*100)/configData.maxUsers)<40 ?"exception" :null : null}/>
                            <h3><strong>{userCont===1 ?`${userCont} Cliente` :`${userCont} Clientes`}</strong></h3>
                        <h3>PAGADO: <strong>${paid}.00 MXN </strong></h3>
                        <h3>RESTANTE: <strong>{amount!==0 ?`${amount-paid}.00 MXN` :null}</strong></h3>
                   </Card.Grid>
                    <Card.Grid  className= "card-item">
                         <RegisterPayment/>
                    </Card.Grid>
                    <Card.Grid className="card-item">      
                        <RegisterCuponCode/>  
                    </Card.Grid>
                </Card>  
                </Col>
                <Col lg={2}/>
            </Row>
          
        </>
    );
}
 
export default AdminHome;