import {Table} from 'antd';
import React, { useEffect, useState } from 'react';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';
import './Payments.scss';

import Helmet from 'react-helmet';

const Payments = () => {
  const [getData, setData]=useState([]);
  const [users, setUsers] = useState();
  const [admins, setAdmins] = useState();
  

  

 useEffect(() => {
  

      firebase.firestore().collection('pagos').where('amount','>', 0).onSnapshot(function(snapshot){
        const payments = snapshot.docs.map( doc =>{
          
            return {
              id: doc.id,
              key: doc.id,
              ...doc.data()
           }
        });
        setData(payments);
      })
      
      firebase.firestore().collection('usuarios').where('priceParty','>',0).onSnapshot(function(snapshot){
        const database = snapshot.docs.map(doc=>{
          return{
            key: doc.id,
            id: doc.id,
            ...doc.data()
          }
        });
        setUsers(database);
      })

      firebase.firestore().collection('administradores').where('email','!=',"").onSnapshot(function(snapshot){
        const database = snapshot.docs.map(doc=>{
          return{
            key: doc.id,
            id: doc.id,
            ...doc.data()
          }
        });
        setAdmins(database);
      })

    
 }, []);

    const data = [];
    if(users&&getData&&admins){
      for(let i=0; i< getData.length ; i++){
        let userEmail = "";
        let adminEmail= "";
        for(let j=0 ; j<users.length ; j++){
          if(users[j].id===getData[i].user){
              userEmail = users[j].email;
          }
        }

        for(let w=0 ; w<admins.length ; w++){
          if(admins[w].id===getData[i].authorizadedBy){
              adminEmail = admins[w].email;
          }
        }

        data.push({
          key: getData[i].id,
          userEmail: userEmail,
          id: getData[i].id,
          amount: `$${getData[i].amount}`,
          concept: getData[i].concept,
          paidDate: getData[i].paidDate,
          adminEmail: adminEmail
        })
      }
    }

 
    const columns = [
        {
          title: 'id',
          width: 100,
          dataIndex: 'id',
          key: 'id',
          fixed: 'left',
        },
        {
          title: 'Cantidad',
          width: 100,
          dataIndex: 'amount',
          key: 'amount',
        },
        {
          title: 'usuario',
          dataIndex: 'userEmail',
          key: 'userEmail',
          width: 150
        },
        {
          title: 'Fecha',
          dataIndex: 'paidDate',
          key: 'paidDate',
          width: 150,
        },{
          title: 'autorizado por',
          dataIndex: 'adminEmail',
          key: 'adminEmail',
          width:150
        }
      ];
      
      

    const heigh = window.innerHeight - 330;
    

    return (  
        <>
        <Helmet>
          <title>Administración de pagos</title>
        </Helmet>
        <h1>Recibos</h1>
        <Table columns={columns} dataSource={data} scroll={{  y: heigh , x: 1000}} />
        </>
    );
}
 
export default Payments;