import React, { useEffect, useState } from 'react';
import SettingsForm from '../../../Components/Forms/SettingsForm';
import {Card, Row, Col} from 'antd';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';

import Helmet from 'react-helmet';

import UseAuth from '../../../Hooks/UseAuth';



const Configuration = () => {
    
    const id = UseAuth().authState.id;
    const [configData, setConfigData] = useState(null);
    const [reloadUsers, setReloadUsers] = useState(false);    


    useEffect(() => {
        firebase.firestore().collection('administradores').doc(id).get().then(function(doc){
            if(doc.data().role==="admin"){
               
                return window.location.href = "/admin";
               
            }else{
                setReloadUsers(true);
                firebase.firestore().collection('config').doc('ZJJa8vl6cdhyUiGQwROt').get().then(function(doc){
                    if(doc.exists){
                        setConfigData(doc.data());
                        
                    }
            
            });
        }
    });
        // eslint-disable-next-line 
    }, []);if(reloadUsers){
        
    return (  
        <>
        <Helmet>
            <title>Configuración del viaje</title>
        </Helmet>
        <Row>
            <Col lg={2}/>
            <Col lg={20}>
                <Card>
                        {configData ? <SettingsForm configData={configData}/> :null}
                </Card>
            </Col>
            <Col lg={2}/>
        </Row>
        </>
    );
    }else{
        return null
    }
}
 
export default Configuration;