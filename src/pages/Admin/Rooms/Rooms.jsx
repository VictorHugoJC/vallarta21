import React, {useEffect, useState} from 'react';
import {Table, Button} from 'antd';
import {Link} from 'react-router-dom';


import firebase from '../../../config/FireBase';
import 'firebase/firestore';
import '../../../Components/Admin/listUser/ListUser.scss';
import Modal from '../../../Components/Modal';

import AddNewRoom from '../../../Components/Forms/addNewRoom';

import Helmet from 'react-helmet';

const Rooms = () => {
  const [getData, setGetData] = useState([]);
  const [users, setUsers] = useState([]);
  const [isVisible, setIsVisible] = useState(false);
  const [content, setContent] = useState();

  const newRoom = () =>{
    setContent(<AddNewRoom setIsVisible={setIsVisible}/>);
    setIsVisible(true)
  }

  const editRoom=(id)=>{
      firebase.firestore().collection('habitaciones').doc(id).get().then(doc=>{
      if(doc.exists){
        
          let userEmail = ["","",""];
          for(let i = 0 ; i<doc.data().users.length; i++){
                if(doc.data().users[i]!==""){
                  firebase.firestore().collection('usuarios').doc(doc.data().users[i]).get().then(userDoc =>{
                    if(userDoc.exists){
                        userEmail[i] = userDoc.data().email;
                      }
                      setContent(<AddNewRoom roomData={{id:doc.id, type: doc.data().type, users: userEmail}}/>);
                   
                  }).then(()=>{
                    setIsVisible(true);
                  })
                }
          }
          
      }
    });
  }
   
  useEffect(()=>{

      firebase.firestore().collection('habitaciones').onSnapshot(function(snapshot){
        const database = snapshot.docs.map(doc=>{
          return{
            key: doc.id,
            id: doc.id,
            ...doc.data()
          }
        });
        setGetData(database);
      })

      firebase.firestore().collection('usuarios').where('priceParty','>',0).onSnapshot(function(snapshot){
        const database = snapshot.docs.map(doc=>{
          return{
            key: doc.id,
            id: doc.id,
            ...doc.data()
          }
        });
        setUsers(database);
      })

    },[]);
    

    const columns = [
      {
        title: 'Habitación',
        width: 100,
        dataIndex: 'id',
        key: 'id',
        fixed: 'left',
        render: (text) => <Link to="/admin/rooms" onClick={()=>editRoom(text)}>{text}</Link>,
      },{
        title: 'tipo',
        width: 150,
        dataIndex: 'type',
        key: 'type',
        fixed: 'left',
      },
        {
          title: 'Usuario 1',
          width: 150,
          dataIndex: 'user1',
          key: 'user1',
        },
        {
          title: 'Usuario 2',
          dataIndex: 'user2',
          key: 'user2',
          width: 150,
        },
        {
          title: 'Usuario 3',
          dataIndex: 'user3',
          key: 'user3',
          width: 150,
        },
      ];
      

      //datos
      const data = [];

      if(getData&&users){
        for (let i = 0; i < getData.length; i++) {
          let userEmail1="";
          let userEmail2="";
          let userEmail3="";

          for(let j= 0 ; j<users.length ; j++){

            if(users[j].id===getData[i].users[0]){
                userEmail1 = users[j].email;
            }
            if(users[j].id===getData[i].users[1]){
              userEmail2 = users[j].email;
            }
            if(users[j].id===getData[i].users[2]){
              userEmail3 = users[j].email;
            }
            }
            data.push({
              key:getData[i].id,
              id:getData[i].id,
              type: getData[i].type,
              user1: userEmail1,
              user2: userEmail2,
              user3: userEmail3
            })
          
          }
      }


    
    const heigh = window.innerHeight - 330;


    return (  
        <>
        <Helmet>
          <title>Administración de Habitaciones</title>
        </Helmet>
        <div className = "list-user__header">

        <h1>Habitaciones</h1>
        <Button type="primary" onClick={newRoom}>Nueva habitacion</Button>
        </div>
        <Table columns={columns} dataSource={data} scroll={{  y: heigh , x: 1000}} />
        <Modal
            title = {"Nueva Habitación"}
            isVisible = {isVisible}
            setIsVisible = {setIsVisible}
        >
          {content}
        </Modal>
        </>
    );
}
 
export default Rooms;