import React, { useEffect, useState } from 'react';
import ListCupon from '../../../Components/Admin/ListCupon';

import firebase from '../../../config/FireBase';
import 'firebase/firestore';

import Helmet from 'react-helmet';


const Cupons = () => {
    const [cuponsActive, setCuponsActive] = useState([]);
    const [cuponsInactive, setCuponsInactive] = useState([]);


    useEffect(() => {

        firebase.firestore().collection('cupones').where('status','==', true).onSnapshot(function(snapshot){
            const active = snapshot.docs.map( doc =>{
                return {
                    id: doc.id,
                    ...doc.data()
                }
            });        
            setCuponsActive(active);
            
        });
        firebase.firestore().collection('cupones').where('status','==', false).onSnapshot(function(snapshot){
            const inactive = snapshot.docs.map( doc =>{
                return {
                    id: doc.id,
                    ...doc.data()
                }
            });        
            setCuponsInactive(inactive);
        });

    }, []);

    return ( 
        <>
        <Helmet>
            <title>Administración de cupones</title>
        </Helmet>
        <ListCupon  cuponsActive={cuponsActive} cuponsInactive={cuponsInactive}/>
        </>
     );
}
 
export default Cupons;