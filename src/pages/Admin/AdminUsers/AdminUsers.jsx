import React, {useState} from 'react';

import UseAuth from '../../../Hooks/UseAuth';
import ListUser from '../../../Components/Admin/listUser';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';
import Helmet from 'react-helmet';


const Users = () => {
    const id = UseAuth().authState.id;
    const [usersActive, setUsersActive] = useState([]);
    const [usersInactive, setUsersInactive] = useState([]);
    const [reloadUsers, setReloadUsers] = useState(false);    

        firebase.firestore().collection('administradores').doc(id).get().then(function(doc){
            if(doc.data().role==="admin"){
               
                return window.location.href = "/admin";
               
            }else{
            
                    setReloadUsers(true);
                    firebase.firestore().collection('administradores').where('status','==', true).onSnapshot(function(snapshot){
                        const ActiveUsers = snapshot.docs.map( doc =>{
                            return {
                                id: doc.id,
                                ...doc.data()
                            }
                        });
                        setUsersActive(ActiveUsers);
                       
                    });
                    firebase.firestore().collection('administradores').where('status','==', false).onSnapshot(function(snapshot){
                        const inactiveUsers = snapshot.docs.map( doc =>{
                            return {
                                id: doc.id,
                                ...doc.data()
                            }
                        });
                        setUsersInactive(inactiveUsers);
                       
                    });
            
            
            
            }
        })

        if(!reloadUsers){
            return null;
        }else{
            
    return (
        <>
        <Helmet>
            <title>Administración de Usuarios</title>
        </Helmet>  
        <div className="users">
            <ListUser superAdmin={true} usersActive = {usersActive} usersInactive = {usersInactive} setReloadUsers = {setReloadUsers}/>
        </div>
        </>
    );
        }
}
 
export default Users;