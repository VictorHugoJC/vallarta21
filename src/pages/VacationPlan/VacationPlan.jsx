import React,{ useEffect} from 'react';
import {Row,Col,Card, Button} from 'antd';
import * as Scroll from 'react-scroll';
import {Link} from 'react-router-dom';
import './VacationPlan.scss'; 
import {DoubleRightOutlined} from '@ant-design/icons';

import clip from '../../assets/video/170724_10_Setangibeach.mp4';
import logoHotel from '../../assets/img/buenaventura-grand-logo.jpg';
import InfoCard from '../../Components/HotelInfoCard';
import MainBannerVideo from '../../Components/MainBannerVideo';

import Helmet from 'react-helmet';


const VacationPlan = () => {
    useEffect(() => {
        Scroll.animateScroll.scrollToTop();
    }, []);
    return ( 
      
      <>
      <Helmet>
          <title>Información del paquete</title>
      </Helmet>
            {
                window.innerWidth<720
                ?<Banner
                title={"FORMA PARTE DE ESTA AVENTURA"}
                link={"vacation-plan-content"}
                subtitle={"Un viaje a tu medida y al Mejor Precio."}/>
                :(<MainBannerVideo video={clip} 
                    title={"FORMA PARTE DE ESTA AVENTURA"}
                    link={"vacation-plan-content"}
                    subtitle={"Un viaje a tu medida y al Mejor Precio."}
                />)
            }
           
            <div className="vacation-plan-content">
                <br/><br/>
                
                    <div className="vacation-plan-content-hotel-info">
                    <Row>
                        <Col lg={4}/>
                        <Col lg={16}>
                            <h1>Disfruta de increibles días en el Hotel</h1>
                            <a href='https://www.hotelbuenaventura.com.mx/es' 
                                target="_blank" rel="noopener noreferrer">
                            <img src={logoHotel} alt=""/>
                            </a>
                        </Col>
                        <Col lg={4}/>
                    </Row>
                    </div>
               
                
                <InfoCard/>
                </div>
                <div className="vacation-plan-content-highlight-plan">
                    <Row>
                        <Col xs={4}/>
                        <Col xs={16}>
                            <Card className="vacation-plan">
                            <h2>¿Qué incluye el paquete?</h2>
                                <CardPlan 
                                    icon={<ion-icon name="restaurant-sharp"></ion-icon>}
                                    description={"Alimentos Incluidos"}
                                />
                                <CardPlan 
                                    icon={<ion-icon name="wine-sharp"></ion-icon>}
                                    description={"Barra Libre"}
                                />
                                <CardPlan 
                                    icon={<ion-icon name="bus-sharp"></ion-icon>}
                                    description={"Transporte Redondo"}
                                />
                                <CardPlan 
                                    icon={<ion-icon name="medkit-sharp"></ion-icon>}
                                    description={"Seguro de Pasajero"}
                                />
                            </Card>
                            
                        </Col>
                        <Col xs={4}/>
                    </Row>
                </div>
                <div className="vacation-plan-content">
                <div className="extra-activities">
                        <h1>ACTIVIDADES EXTRAS</h1>
                        <br/><br/>
                        <div className="extra-activities-text">
                        <p>Disfruta de la vida Nocturna de Puerto Vallarta<br/>
                            o de un increíble paseo en barco por un costo extra<br/>
                            a increible precio</p>
                        </div>
                        <Link to="/process"><Button type="primary" className="extra-activities-btn">Cotizar Viaje</Button></Link>
                </div>
            </div>
             

        </>
    );
}
export default VacationPlan;

function CardPlan ({icon, description}){
   return( <Card.Grid hoverable={false} className="vacation-plan-item">
        {icon}<br/>
        {description}
    </Card.Grid>);
                             
}

function Banner ({title,subtitle,link}){
    return(
        <>
       

        <div className="banner-img">
            <Row>
                <Col xs={4}/>
                < Col xs={16}>
                <h1>{title}</h1>
                <h2>{subtitle}</h2><br/>
                {link 
                ?  <Scroll.Link to={link} smooth={true} offset={50} duration={800} delay={200}><DoubleRightOutlined/></Scroll.Link> 
                : null}
                </Col>
                <Col xs={4}/>

            </Row>
        </div>
        </>
    )
}