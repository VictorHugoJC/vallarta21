import React from 'react'
import { Result } from 'antd';
import Helmet from 'react-helmet';

const Error404 = () => {

    return (  
        <>
        <Helmet>
          <title>Error al buscar la página</title>
        </Helmet>
        <Result
          status="404"
          title="ERROR 404"
          subTitle="Lo sentimos, la dirección que deseas no existe"
          extra={"por favor, navega con el menú a una pagina existente"}
        />
  </>

    );
}
 
export default Error404;