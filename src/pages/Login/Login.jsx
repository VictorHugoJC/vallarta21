import React,{Fragment, useEffect} from 'react'
import * as Scroll from 'react-scroll';
import './Login.scss';
import {Layout, Card} from 'antd';
import LoginForm from '../../Components/Forms/LoginForm';
import {Particles} from 'react-particles-js';
import firebase  from '../../config/FireBase';
import Helmet from 'react-helmet';
import'firebase/auth';


const Login = () => {
    useEffect(() => {
        Scroll.animateScroll.scrollToTop();
        firebase.auth().signOut();
    }, []);

    return (  
        <Fragment>
            <Helmet>
                <title>Iniciar Sesión</title>
            </Helmet>
            <div className="login-background">
                <Particles 
                
                params={{
                    particles: {
                        line_linked: {
                            shadow: {
                                enable: true,
                                color: "#3CA9D1",
                                blur: 5
                            }
                        }
                    }
                }}
                style={{
                backgroundcolor: 'black',
                
                }}
            />
            </div>
            <div className="login-content">
            <Layout className="sign-in">
        <Layout.Content className = "sign-in__content">
        

            <div className="sign-in__content-tabs">
            <Card type="card">
                <h1>LogIn</h1>
                <LoginForm/>
                
            </Card>
            </div>
        </Layout.Content>

        </Layout>
            </div>
        </Fragment>
    );

}

export default Login;