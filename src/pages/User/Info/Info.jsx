import React from 'react';
import './Info';
import UseAuth from '../../../Hooks/UseAuth';
import UserInfo from '../../../Components/UserInfo';

import Helmet from 'react-helmet';

const Info = () => {
    const {authState} = UseAuth();

    return (
        <>
        <Helmet>
          <title>Información de usuario</title>
        </Helmet>
        
        <UserInfo   id={authState.id}/>
        </>
      );
}
 
export default Info;