import React, { useEffect, useState } from 'react'
import UseAuth from '../../Hooks/UseAuth';
import CountDown from '../../Components/CountDown';
import ProgressBarSteps from '../../Components/ProgressBarSteps';
import {Card, Spin} from 'antd';
import firebase from '../../config/FireBase';

import Helmet from 'react-helmet';

import './User.scss';

const User = () => {
    const {authState} = UseAuth();
    const [total, setTotal] = useState(null);
    const [paid, setPaid] = useState(null);
    const [dateTrip, setDateTrip] = useState(null);
    const id = authState.id;

   

        useEffect(()=>{
            if(id){
                firebase.firestore().collection('usuarios').doc(id).get().then(function(doc){
                    if(doc.exists){
                        setTotal(doc.data().price);
                    }
                }).then(()=>{
                    firebase.firestore().collection('pagos').where('user',"==",id).onSnapshot(function(snapshot){
                        const data= snapshot.docs.map(doc=>{
                            const payment = doc.data();
                            return payment;
                        });
                        setPaid(data);
                    })
                    
                
                })

                
            }

            firebase.firestore().collection('config').doc('ZJJa8vl6cdhyUiGQwROt').get().then(doc=>{
                if(doc.exists){
                    setDateTrip(doc.data().tripDate);
                }
            })
            // eslint-disable-next-line 
        },[])
        


    return ( 
        <>
        <Helmet>
            <title>Mi cuenta</title>
        </Helmet>
        <div className="user-home">
        <h1>Hola {authState.name}</h1>
        <Card>
            <div className="user-home__count-down">
                {dateTrip!==null
                    ?<CountDown   time={`${dateTrip[0]}-${dateTrip[1]<10 ?`0${dateTrip[1]}` :dateTrip[1]}-${dateTrip[2]<10 ?`0${dateTrip[2]}` :dateTrip[2]}T01:00:03`}/>
                    : <Spin tip="cargando" size="large"/>
                }
            </div>
            {
                total&&paid
                ?<ProgressBar total={total} paid={paid}/>
                :null
            }
        </Card>
        </div>
     </>
     );
}
 
export default User;

function ProgressBar({total, paid}){
    
    const [pairdPorcent, setPaidPorcent] = useState(null);

   
    useEffect(()=>{
        
        if(total&&paid){
            let totalPaid = 0;
            for(let i = 0 ; i<paid.length; i++){
                    totalPaid+=paid[0].amount;
            }
            setPaidPorcent(Math.round((totalPaid*100)/total))
        }
        // eslint-disable-next-line 
    },[])

    return(
        <>
        {
            pairdPorcent
            ?(
                <div className="user-home__progress-bar">
                    <h1     className="payment">PAGADO AL {pairdPorcent}%</h1>
                    <Card>
                        <ProgressBarSteps   porcentBar={pairdPorcent}/>
                    </Card>
                </div>)
            : <h2>Se ha registrado con éxito, un administrador se contactará con usted por correo electrónico</h2>
        }
        </>
    )
    

}