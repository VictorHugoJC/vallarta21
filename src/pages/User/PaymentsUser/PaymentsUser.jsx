import React, { useEffect, useState } from 'react';
import UseAuth from '../../../Hooks/UseAuth';
import PaymentInfo from '../../../Components/PaymentInfo';

import Helmet from 'react-helmet';

const PaymentsUser = () => {
    const heigh = window.innerHeight - 450;
    const width = window.innerWidth  - 370;

    const {authState} = UseAuth();
    const [userId, setuserId] = useState(null);

    useEffect(()=>{
        if(authState.id){
            setuserId(authState.id);
        }
        // eslint-disable-next-line 
    }, [])


   
    return(  
        <>
        <Helmet>
            <title>Información de pagos</title>
        </Helmet>
       {userId
            ? (<PaymentInfo id = { userId} heigh={heigh} width={width}/>)
            :(null)
        }
        </> 
);

}
 
export default PaymentsUser;