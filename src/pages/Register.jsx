import React from 'react'
import Process from './Process';
import {Redirect, Route} from 'react-router-dom';
import UseQuotation from '../Hooks/UseQuotation';
import RegisterForm from '../Components/Forms/RegisterForm';
import {Steps, Row, Col} from 'antd';

import InfoTrip from '../Components/Forms/Quotation/InfroTrip';

const Register = (props) => {
   const {quotationState} = UseQuotation();
   
   
   const {Step} = Steps;
    if(!quotationState.room){//Validar que ya viene de proceso
        return (  
            <>
            <Route path="/process" component={Process}/>
            <Redirect to="/process" />
            </>
        );
    }else{
        return (
            <>  
            <Row>
    <Col lg={4}/>
            <Col lg={16}>
               <InfoTrip quotation={quotationState} button={false}/>
            </Col>
            
        <Col lg={4}/>
        <Col xs={4}/>
            <Col xs={16}>
                <Steps current={1}>
                <Step key={0} title="Cotiza" description="Selecciona tu paquete" />
                <Step key={1} title="Registrate"  description="Aquí comienza tu proceso" />
                <Step key={2} title="Viaja" description="Disfruta, te lo mereces"  />
                </Steps>
            </Col>
            
        <Col xs={4}/>
       
    </Row>
    <Row>
         <Col xs={4}/>
        <Col xs={16}>
            <RegisterForm/>
        </Col>
        <Col xs={4}/>
    </Row>
            </>
        );
    }
}
 
export default Register;