import React, {Fragment} from 'react';
import ReactPlayer from 'react-player'; 
import './MainBannerVideo.scss';
import {Col,Row} from 'antd';
import * as Scroll from 'react-scroll';
import {DoubleRightOutlined} from '@ant-design/icons';

const MainBannerVideo = ({video, title,subtitle,link}) => {
    const width = window.innerWidth-18;
    const height = width/1.7777778; 

    return (  
        <Fragment>
                <div className="Main-Banner-Video-banner">
                <ReactPlayer 
                    height={`${height}px`}
                    width={`${width}px`}
                    muted={true} playing={true}
                    loop={true}
                    url= {[{src: video, type: 'video/webm'}]}
                    
                    />  
            </div> 
            <div className="Main-Banner-Video-shape"/>
            <div className="Main-Banner-Video-short-video"/>
            <div className="Main-Banner-Video-text">
                    <Row>
                        <Col xs={4}/>
                        < Col xs={16}>
                        <h1>{title}</h1>
                        <h2>{subtitle}</h2><br/>
                        {link 
                        ?  <Scroll.Link to={link} smooth={true} offset={50} duration={800} delay={200}><DoubleRightOutlined/></Scroll.Link> 
                        : null}
                        </Col>
                        <Col xs={4}/>

                    </Row>
            </div>
    </Fragment>
    );
}
 
export default MainBannerVideo;