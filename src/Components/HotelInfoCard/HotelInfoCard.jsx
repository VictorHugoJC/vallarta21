import React,{Fragment} from 'react';
import {Row,Col,Card, Button} from 'antd';
import './HotelInfoCard.scss';
import img1 from '../../assets/img/rooms-hotel-info.png';
import img2 from '../../assets/img/restaurants-hotel-info.png';
import img3 from '../../assets/img/bares-hotel-info.png';
import img4 from '../../assets/img/pool-hotel-info.png';
import img5 from '../../assets/img/activities-hotel-info.png';
import img6 from '../../assets/img/places-hotel-info.png';
import Meta from 'antd/lib/card/Meta';




const HotelInfoCard = () => {
    return (  
        <Fragment>
            <Row>
                <Col lg={24} className="information-">
                    <h2>Información sobre el Hotel</h2>
                </Col>
                <Col lg={4}/>
                <Col lg={16}>
                    <Row>
                    <CardInfo
                        title={"HABITACIONES"}
                        img={img1}
                        description={`Hermosas, confortables y modernas.Las habitaciones cuentan con mini-bar que
                        diario es surtido.`}
                        link={"https://www.hotelbuenaventura.com.mx/es/habitaciones"}
                        key={0}
                    />
                    <CardInfo
                        title={"RESTAURANTES"}
                        img={img2}
                        description={`Excelente experiencia gastronómica
                        de sabores mexicanos e internacionales
                        en 3 restaurantes diferentes.`}
                        link={"https://www.hotelbuenaventura.com.mx/es/todo-incluido"}
                        key={1}
                        />
                    <CardInfo
                        title={"BARES"}
                        img={img3}
                        description={`El hotel cuenta con 3 bares: en la alberca,
                        cerca de la playa y en el Lobby para
                        tu mayor comoidad`}
                        link={"https://www.hotelbuenaventura.com.mx/es/todo-incluido"}
                        key={2}
                        />
                    <CardInfo
                        title={"INSTALACIONES"}
                        img={img4}
                        description={`Ubicadi justo frente al mar, el hotel cuenta
                        con albercas, camastros y excelentes
                        instalaciones para mejorar tu estadía.
                        `}
                        link={"https://www.hotelbuenaventura.com.mx/es/galeria"}
                        key={3}
                        />
                    <CardInfo
                        title={"AMENIDADES"}
                        img={img5}
                        description={`Gimnasio, Spa, Paseo en bicicleta hasta por
                        2 horas, deportes, recorrido en kayak,
                        clases de baile, wifi entre muchas más .`}
                        link={"https://www.hotelbuenaventura.com.mx/es/amenidades"}
                        key={4}
                        />
                    <CardInfo
                        title={"ATRACCIONES"}
                        img={img6}
                        description={`Debido a la excelente ubicación del hotel,
                        descubre los excelentes puntos de
                        turismo que Puerto Vallarta te ofrece para mejorar tu estadía`}
                        link={"https://www.hotelbuenaventura.com.mx/es/atraccciones"}
                        key={5}
                        />
                    
                    </Row>
                </Col>
            </Row>
        </Fragment>
    );
}
 
export default HotelInfoCard;

function CardInfo({title,img,description,link}){
    return(

        <Col md={8}>
            <Card 
                cover={
                    <img src={img} alt={title}/>
                }>
                <Meta title={title} description={description}/><br/>
                <Button type="primary" href="/" target="_blank" 
                    className="btn-see-more" rel="noopener referrer">
                        Ver más
                </Button>
            </Card>
        </Col>

    );
}