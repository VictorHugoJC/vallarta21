import React from 'react';
import Countdown from 'react-countdown';
import './CountDown.scss';
import {Card} from 'antd';

const CountDown = ({time}) => {

    const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
          // Render a completed state
          return <h1>Gracias por ser parte de esta experiencia</h1>
        } else {
          // Render a countdown
          return (    
            <Card className="count-down__card">
                <h1>Tiempo Restante</h1>
                <Card.Grid className="count-down__card-date"><h2>{days<10 ?`0${days}` :`${days}`}</h2><h2>Días</h2></Card.Grid>
                <Card.Grid className="count-down__card-date"><h2>{hours<10 ?`0${hours}` :`${hours}`}</h2><h2>Hrs</h2></Card.Grid>
                <Card.Grid className="count-down__card-date"><h2>{minutes<10 ?`0${minutes}` :`${minutes}`}</h2><h2>Min</h2></Card.Grid>
                <Card.Grid className="count-down__card-date"><h2>{seconds<10 ?`0${seconds}` :`${seconds}`}</h2><h2>Seg</h2></Card.Grid>
                
             </Card>    
          );         
      }
    }  

    return (  
        <>

            <Countdown
                date={Date.parse(time)}
                renderer={renderer}
            />  
           
        </>
    );
}
 
export default CountDown;