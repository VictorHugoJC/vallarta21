import React, {useState, Result} from 'react'
import Modal from './Modal';

const SuccesFullMessage = ({isVisible, setIsVisible, title, subTitle}) => {
  
    const [content, setcontent] = useState("");

    setcontent(<Result 
        status="success"
        title={title}
        subTitle={subTitle}
   />);
    
    return (  
        <Modal isVisible={isVisible} setIsVisible={setIsVisible}>{content}</Modal>
    );
}
 
export default SuccesFullMessage;