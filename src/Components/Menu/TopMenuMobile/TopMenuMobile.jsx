    import React, {useEffect, useState} from 'react';
    import {slide as Menu} from 'react-burger-menu'
    import'./TopMenuMobile.scss';
    import {MenuOutlined} from '@ant-design/icons';
    import {Link} from 'react-router-dom';
    import Logo from '../../../assets/img/Logo-covitrip21.png';
    import firebase from '../../../config/FireBase';
    import'firebase/auth';
    
    
    const TopMenuMobile = () => {
        const [userLogged, setUserLogged] = useState(null);

        useEffect(() => {
            firebase.auth().onAuthStateChanged(function(user){
                 if(!user){
                    
                     setUserLogged({
                         text: 'Iniciar Sesión',
                         linkTo:'/login'
                     });
                 }else{
                   firebase.firestore().collection('administradores').doc(user.uid).get().then(function(doc){
                        if(doc.exists){
                            
                            setUserLogged({
                                text:"Mi cuenta",
                                lintTo:'/user'
                            })
                        }else{
                            
                            setUserLogged({
                                text:"Mi cuenta",
                                linkTo:'/user'
                            })
                        }
                   })
                 }
            })
         }, [])
      
        
            return (  
           <div className="menu-top-mobile">
               <div className="menu-top-mobile_left">
                   
                <button className="menu-top-mobile-button">
                    {<MenuOutlined className="icon" />}
                </button>
                <Menu left >
                    
                   
                <Link  to='/' className="menu-item"> Home </Link>    
                <Link  to='/process' className="menu-item"> Paquete </Link>            
                <Link  to='/vacation-plan' className="menu-item"> Proceso </Link>
                           
                <div className="Login">
                        {userLogged ?(<Link to ={userLogged.linkTo} className="button">{userLogged.text}</Link>)
                            :null}
                    </div>
    
                </Menu>
               </div>
               <div className="menu-top-mobile_right">
                    <Link to="/"><img className="menu-top-mobile_img" alt="" src={Logo}/></Link>
               </div>
           </div>
        );
    }
     
    export default TopMenuMobile;