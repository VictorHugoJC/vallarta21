import React, {useEffect, useState} from 'react';
import {Menu,Row,Col,Button} from 'antd';
import {Link} from 'react-router-dom';
import './TopMenu.scss';
import LOGO from '../../../assets/img/Logo-covitrip21.png';
import firebase from '../../../config/FireBase';
import'firebase/auth';



const TopMenu = () => {
    const [userLogged, setUserLogged] = useState(null);
    useEffect(() => {
        firebase.auth().onAuthStateChanged(function(user){
             if(!user){
                
                 setUserLogged({
                     text: 'Iniciar Sesión',
                     linkTo:'/login'
                 });
             }else{
               firebase.firestore().collection('administradores').doc(user.uid).get().then(function(doc){
                    if(doc.exists){
                        setUserLogged({
                            text:"Mi cuenta",
                            linkTo:'/admin'
                        })
                    }else{
                        
                        setUserLogged({
                            text:"Mi cuenta",
                            linkTo:'/user'
                        })
                    }
               })
             }
        })
     }, [])
  
    
    return (  
        
        <Row>  
        <Col lg={4}/>
        <Col  lg={16}>
                <Menu className="menu-top" mode="horizontal">
                    <Menu.Item className="menu-top__logo" key={0} >
                        <Link to="/" ><img alt="Covitrip21" src={LOGO}/> </Link>
                    </Menu.Item>
                <div className="menu-top__space" key={11}/>
                    <Menu.Item className="menu-item" key={1}>
                        <Link to="/">Home</Link>
                    </Menu.Item>
                    <Menu.Item className="menu-item" key={2}>
                        <Link to="/vacation-plan">Paquete</Link>
                    </Menu.Item>
                    <Menu.Item className="menu-item" key={3}>
                        <Link to="/process">Proceso</Link>
                    </Menu.Item>
                    <Menu.Item className="menu-item-login" key={4}>
                       {userLogged
                       ?    ( <Link to={userLogged.linkTo}>
                        <Button htmlType="submit" className="menu-top__button">
                        {userLogged.text}
                        </Button>
                        </Link>)
                        : null
                       }
                    </Menu.Item>
                </Menu>
            </Col>
            <Col lg={4}/>

        </Row>
       );
}
 
export default TopMenu;