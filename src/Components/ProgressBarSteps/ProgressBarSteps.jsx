import React from 'react';
import { ProgressBar, Step } from "react-step-progress-bar";
import "react-step-progress-bar/styles.css";

import './ProgressBarSteps.scss';

const ProgressBarSteps = ({porcentBar}) => {

    const width = window.innerWidth;
    return (  
        <>
        <ProgressBar
        percent={porcentBar}
        fillBackground="linear-gradient(to right, #fefb72, #f0bb31)"
      >
          <Step transition="scale">
          {({ accomplished }) => (
            <img
                className="img"
                alt=""
                style={{ filter: `grayscale(${accomplished ? 0 : 80}%)` }}
                width={width<350 ?"15" :"30"}
                src="https://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Beach-icon.png"
            />
          )}
        </Step>
        <Step transition="scale">
          {({ accomplished }) => (
            <img
                alt=""
                style={{ filter: `grayscale(${accomplished ? 0 : 80}%)` }}
                width={width<350 ?"15" :"30"}
                src ="https://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Beach-icon.png"
            />
          )}
        </Step>
        <Step transition="scale">
          {({ accomplished }) => (
            <img
                alt=""
                style={{ filter: `grayscale(${accomplished ? 0 : 80}%)` }}
                width={width<350 ?"15" :"30"}
                src="https://icons.iconarchive.com/icons/webalys/kameleon.pics/512/Beach-icon.png"
            />
          )}
        </Step>
          
      </ProgressBar>
        </>
    );
}
 
export default ProgressBarSteps;