import React, { useEffect } from 'react';
import { Form, Button,DatePicker, notification, InputNumber, Popconfirm} from 'antd';
import { useState } from 'react';
import moment from 'moment';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';

const SettingsForm = ({configData}) => {
    const [dataForm, setDataForm] = useState({});
    useEffect(()=>{
        setDataForm({
            tripDate: configData.tripDate,
            maxRooms: configData.maxRooms,
            maxUsers: configData.maxUsers,
            tripleRoomPrice: configData.tripleRoomPrice,
            dobleRoomPrice:configData.dobleRoomPrice,
            antro:configData.antro,
            barra:configData.barra,
            bote:configData.bote
        
        })
    },[]);

    const onChangeDate = (e) =>{
        if(e){
            setDataForm({
                ...dataForm,
                tripDate: [e.year(),e.month()+1,e.date()]
            })
        }
    }
    const onchange=(value, name)=>{
        setDataForm({...dataForm,
                [name]:value
            });
    }
    const onsubmit =()=>{
        const{tripDate} = dataForm;
    //validar fecha        
            if(moment(tripDate).diff(moment())<0){
                return notification["error"]({
                    message: "La fecha es incorrecta"
                })
            }else{
                firebase.firestore().collection('config').doc('ZJJa8vl6cdhyUiGQwROt').set(dataForm).then(()=>{
                    return notification["success"]({
                        message: "Se han actualizado los datos"
                    })
                }).catch(()=>{
                    return notification["error"]({
                        message: "Ocurrió un error al actualizar la información"
                    })
                })
            }
    }
    return (  
        <>
            <h2>Configuración</h2>
                <Form
                
                    labelCol={{
                        span: 10,
                    }}
                    wrapperCol={{
                        span: 8,
                    }}
                    
                >
                    <Form.Item label="Numero máximo de clientes" className="select-item" >
                        <InputNumber    min={10}  value={dataForm.maxUsers} onChange={e=>onchange(e, "maxUsers")}/>
                    </Form.Item>
                    <Form.Item label="Numero máximo de habitaciones" className="select-item" >
                        <InputNumber    min={10} value={dataForm.maxRooms} onChange={e=>onchange(e, "maxRooms")}/>
                    </Form.Item>
                    <Form.Item label="Costo habitación doble ($MXN)" className="select-item" >
                        <InputNumber    min={1000} value={dataForm.dobleRoomPrice} onChange={e=>onchange(e, "dobleRoomPrice")}/>
                    </Form.Item>
                    <Form.Item label="Costo habitación triple ($MXN)" className="select-item" >
                        <InputNumber    min={1000} value={dataForm.tripleRoomPrice} onChange={e=>onchange(e, "tripleRoomPrice")}/>
                    </Form.Item>
                    <Form.Item label="Costo Barra libre ($MXN)" className="select-item" >
                        <InputNumber    min={100} value={dataForm.barra} onChange={e=>onchange(e, "barra")}/>
                    </Form.Item>
                    <Form.Item label="Costo Cover ($MXN)" className="select-item" >
                        <InputNumber    min={100} value={dataForm.antro} onChange={e=>onchange(e, "antro")}/>
                    </Form.Item>
                    <Form.Item label="Costo Bote ($MXN)" className="select-item" >
                        <InputNumber    min={100} value={dataForm.bote} onChange={e=>onchange(e, "bote")}/>
                    </Form.Item>

                            <Form.Item label="Fecha del viaje" className="select-item"> 
                        <DatePicker  picker="date" size="large" 
                            value={dataForm.tripDate 
                                    ?(moment(`${dataForm.tripDate[0]}-${dataForm.tripDate[1]}-${dataForm.tripDate[2]}`,'YYYY-MM-DD')) :null} onChange={e=>onChangeDate(e)}/>
                    </Form.Item> 
            
                </Form>
                <Popconfirm
                    title="¿Seguro que deseas realizar cambios?"
                    onConfirm={onsubmit}
                    okText="Si"
                    cancelText="No"
                >
                <Button type="primary" >Guardar Configuracion</Button>
                </Popconfirm>
          </>     
    );
}
 
export default SettingsForm;