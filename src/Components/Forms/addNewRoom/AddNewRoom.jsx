import React, { useEffect } from 'react';
import  {Button, Input, notification, Popconfirm,Form, Select}  from 'antd';
import {isEmpty, set, size} from 'lodash';
import { useState } from 'react';
import {validateEmail} from '../../../config/utils/Validations';


import firebase from '../../../config/FireBase';
import 'firebase/firestore';

let success=0;
let successEdit=0;

const AddNewRoom = ({idRoom, roomData, setIsVisible}) => {
    
    
    const [roomStored, setRoomStored] = useState(roomData ?roomData :null);
    const [bandera, setBandera]=useState(true);
    const [IdUsers, setIdUsers]=useState([])
    const [newAdmin, setNewAdmin] = useState( {
        id: "",
        type: "doble",
        user1:"",
        user2:"",
        user3:""
    });

    useEffect(()=>{
        verify();
    },[success]);
    
    
    useEffect(()=>{
        verify();
    },[successEdit]);
    
    useEffect(()=>{
        verify();
    },[IdUsers]);

    useEffect(()=>{
        if(roomData){
            setRoomStored(roomData);
            if(roomData.type==="doble"){
                setBandera(true)
            }else{
                setBandera(false)
            }
        }

    },[roomData]);
    
    
    const updateRoom=()=>{
        successEdit=0;
        setIdUsers([])

        console.log(roomStored);
        let {id, users} = roomStored;
        let user1 = users[0];
        let user2 = users[1];
        let user3;
        if(users[2]){
             user3 = users[2];
        }

        if(isEmpty(id)||size(id)<3){
            return notification["error"]({
                message: "por favor asigne un nombre válido"
            });
        }else{
           

            if(user1===""&&user2===""&&user3===""){
                return notification["error"]({
                    message: "Debe de escribir al menos un email"
                });         
            }else{
                if((user1!==""&&user2!==""&&user1===user2)||
                (user1!==""&&user3!==""&&user1===user3)||
                (user2!==""&&user3!==""&&user2===user3)){
                        return notification["error"]({
                            message: "No pueden repetirse los emails"
                        });
                
                }else{
                     if(user1!==""){
                        getIdUser(user1, "cliente 1");
                    }else{
                        ++successEdit;
                    }
                    if(user2!==""){
                        getIdUser(user2, "cliente 2");
                        
                    }else{
                        ++successEdit;
                    }
                    if(user3!==""&&user3){
                        getIdUser(user3, "cliente 3");
                        
                    }else{
                        ++successEdit;
                    }
                }
                firebase.firestore().collection('habitaciones').doc(id).get().then(function(doc){
                    if(doc.exists){
                        console.log(doc.id);
                        console.log(id);
                        if(doc.id!==id){
                            return notification["error"]({
                                message: "Ya existe una habitación con ese nombre"
                            })
                        }else{
                            ++successEdit;
                            verify();    
                        }
                    }else{
                        ++successEdit;
                        verify();
                    }
                });
        }


        }  
    }

    const replace=async (user, name, email)=>{
        let newRegister = false;
        
            await firebase.firestore().collection('habitaciones').where("users","array-contains",user).onSnapshot(function(snapshot){
                const registered = snapshot.docs.map(doc=>{
                    return doc.id
                })
                const search=()=>{
                    if(registered.length===0){
                        newRegister=true;
                        console.log(user)
                        setIdUsers(oldArray => [...oldArray, user])
                        ++successEdit
                        ++success
                        verify();
                    }else{
                       if(!newRegister){
                        return notification["error"]({
                            message: `El ${name} ya tiene habitación asignada`
                           })
                        }
                    }
                }
                if(roomData){
                    let sameUser = false;
                 
                    for(let i=0; i<roomData.users.length;i++){
                        
                        if(roomData.users[i]===email){
                            sameUser=true;
                            break;
                        }
                    }
                    if(sameUser){

                        setIdUsers(oldArray => [...oldArray, user])
                        ++successEdit
                        verify();
                    }else{
                        search();
                    }
                }else{
                    search();
                }
            })

    }
    const verify=( )=>{
        
        if(roomData){
            if(successEdit===4){
                console.log(IdUsers);
                successEdit = 0;
                if(IdUsers.length>0){
                    firebase.firestore().collection('habitaciones').doc(roomData.id).delete().then(()=>{
                        firebase.firestore().collection('habitaciones').doc(roomStored.id).set({
                            type: roomStored.type,
                            users: IdUsers
                        }).then(()=>{
                            setIdUsers([])
                            return notification["success"]({
                                message: "Se ha actualizado exitosamente la habitación"
                            })    
                        }) 
                    })
                }
            }
    
        }else{
            if(success===4){
                success=0;
                
                firebase.firestore().collection('habitaciones').doc(newAdmin.id).set({
                    type: newAdmin.type,
                    users: IdUsers
                }).then(()=>{
                    
                    setIsVisible(false);
                    setNewAdmin({
                        id: "",
                        type: "doble",
                        user1:"",
                        user2:"",
                        user3:""
                    });
                    setIdUsers([]);
                    notification["success"]({
                        message: "Se ha registrado exitosamente la habitación"
                    })
                    return window.location.href='/admin/rooms';    
                })        
            }
        }
        
        
    }
    
    
    const getIdUser=(email, name)=>{
        firebase.firestore().collection('usuarios').where('email','==',email).onSnapshot(function(snapshot){
        const userM = snapshot.docs.map( doc =>{
            return doc.id
            
        });        
        if(userM[0]){
            replace(userM[0],name,email);
            verify();
        }else{
            return notification["error"]({
                message: `el ${name} no existe`
            }); 
        }
    })
}

   const onSubmit=async()=>{
       success=0;
        setIdUsers([])
        const {id, user1, user2, user3} =   newAdmin;
        
        if(isEmpty(id)||size(id)<3){
            return notification["error"]({
                message: "por favor asigne un nombre válido"
            });
        }else{
           

            if(user1===""&&user2===""&&user3===""){
                return notification["error"]({
                    message: "Debe de escribir al menos un email"
                });         
            }else{
                if((user1!==""&&user2!==""&&user1===user2)||
                (user1!==""&&user3!==""&&user1===user3)||
                (user2!==""&&user3!==""&&user2===user3)){
                        return notification["error"]({
                            message: "No pueden repetirse los emails"
                        });
                
                }else{
                     if(user1!==""){
                        getIdUser(user1, "cliente 1");
                    }else{
                        ++success;
                    }
                    if(user2!==""){
                        getIdUser(user2, "cliente 2");
                        
                    }else{
                        ++success;
                    }
                    if(user3!==""){
                        getIdUser(user3, "cliente 3");
                        
                    }else{
                        ++success;
                    }
                }
                firebase.firestore().collection('habitaciones').doc(newAdmin.id).get().then(function(doc){
                    if(doc.exists){
                        return notification["error"]({
                            message: "Ya existe una habitación con ese nombre"
                        })
                    }else{
                        ++success;
                        verify();
                    }
                });
        }


        }  
   }

   const changeType=(e)=>{
    if(roomData){
        if(e==="doble"){
            setBandera(true);
            setRoomStored({...roomStored, type:e, sers:[roomStored.users[0],roomStored.users[1],""]})
       }else{
        setBandera(false);
            setRoomStored({...roomStored, type:e});
       }
    }else{
        
        if(e==="doble"){
            setBandera(true);
            setNewAdmin({...newAdmin, type:e, user3:""})
       }else{
            setBandera(false);
            setNewAdmin({...newAdmin, type:e});
       }
    }
        
   }

 


    return (  
         
            <> 
                    <Form
                    
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                        
                    >
                    <Form.Item label="Nombre: "  className="select-item" >
                        <Input  onChange={e=>roomData ?setRoomStored({...roomStored, id:e.target.value}) :setNewAdmin({...newAdmin, id: e.target.value})} value={roomData ?roomStored.id :newAdmin.id}/>
                    </Form.Item>
                    <Form.Item label= "tipo">

                        <Select value={roomStored ?roomStored.type :newAdmin.type}  onChange={e=>changeType(e)}>
                            <Select.Option value="doble">Doble</Select.Option>
                            <Select.Option value="triple">Triple</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="cliente 1: "  className="select-item" >
                        <Input type="email"placeholder="correo electrónico" 
                            onChange={e=>roomData ?setRoomStored({...roomStored, users:[e.target.value, roomStored.users[1],roomStored.users[2]]}) 
                                                   :setNewAdmin({...newAdmin, user1: e.target.value})} 
                            value={roomData ?roomStored.users[0] :newAdmin.user1 }/>
                    </Form.Item>

                    <Form.Item label="cliente 2: "  className="select-item" >
                        <Input type="email" placeholder="correo electrónico" 
                                onChange={e=>roomData ?setRoomStored({...roomStored, users:[roomStored.users[0],e.target.value,roomStored.users[2]]}) 
                                                        :setNewAdmin({...newAdmin, user2: e.target.value})} 
                                value={roomData ?roomStored.users[1] :newAdmin.user2 }/>
                    </Form.Item>

                    <Form.Item label="cliente 3: "  className="select-item" >
                        <Input type="email" placeholder="correo electrónico" disabled={bandera} 
                                onChange={e=>roomData ?setRoomStored({...roomStored, users:[roomStored.users[0],roomStored.users[1],e.target.value]})
                                                     :setNewAdmin({...newAdmin, user3: e.target.value})} 
                                value={roomData ?roomStored.users[2] :newAdmin.user3 }/>
                    </Form.Item>
                    

                        
                    </Form>

                    <Popconfirm
                    title="¿Esta seguro de registrar la habitación?"
                    onConfirm={roomData ?updateRoom :onSubmit}
                    okText="Si"
                    cancelText="No"        
                >
                    <Button style={{width:"100%"}} type="primary">
                        {roomData 
                            ?"Actualizar Habitación "
                            :"Registrar Habitación "
                        }  </Button>
                </Popconfirm>
                   
            </>
        );
    
}
 
export default AddNewRoom;