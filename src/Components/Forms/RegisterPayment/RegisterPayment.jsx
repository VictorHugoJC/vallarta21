import React,{useState} from 'react';
import {Form, Button, Input, notification, InputNumber, Popconfirm} from 'antd';
import {isEmpty, size} from 'lodash';
import {validateEmail} from '../../../config/utils/Validations';


import UseAuth from '../../../Hooks/UseAuth';
import moment from 'moment';

import firebase from '../../../config/FireBase';
import 'firebase/firestore';


const RegisterPayment = () => {
    const shortid = require('shortid');

    const [payment, setPayment] = useState({
        email:"",
        amount:100,
        concept: "",
    });

    const {authState} = UseAuth();
    const idAdmin = authState.id;
    const {email, concept, amount} = payment;

    const updatePayment =(id, date)=>{
        firebase.firestore().collection('pagos').doc(shortid.generate()).set({
            amount: amount,
            authorizadedBy: idAdmin,
            concept: concept,
            paidDate: date,
            user: id
        }).then(()=>{   
                setPayment({})
                return notification["success"]({
                    message: "Se ha registrado exitosamente el pago"
                })
                    })
    }
    
    const onSubmit=()=>{
        const date = `${moment().year()}/${moment().month()+1}/${moment().date()}`;
        if(isEmpty(email)||isEmpty(concept)||amount===null){
            return notification["error"]({
                message: "Todos los campos son necesarios"
            });
        }else{
            if(amount===100){
                return notification["error"]({
                    message: "El pago debe ser mayor de $100.00 MXN"
                });
            }
            if(!validateEmail(email)){
                return notification["error"]({
                    message: "El email no es válido"
                });
            }else{
                if(size(concept)<4){
                    return notification["error"]({
                        message: "El concepto no es muy especifico"
                    })
                }else{
                    firebase.firestore().collection('usuarios').where('email','==',email).onSnapshot(function(snapshot){
                        const user = snapshot.docs.map( doc =>{
                            return {
                                id: doc.id,
                                ...doc.data()
                            }
                        });
                        if(user[0]){
                                
                                if(user[0].status){
                                    updatePayment(user[0].id, date);
                                }else{
                                    return notification["error"]({
                                        message: "El usuario se encuentra deshabilitado"
                                    });
                                }
                        }else{
                            return notification["error"]({
                                message: "No se existe algún usuario con ese correo electrónico"
                            });
                        }
                    })
                }
            }
        }
    }
    
    return (  
        <>
            <h2>Registrar Pago</h2>
                <Form
                
                    labelCol={{
                        span: 12,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    
                >
                    <Form.Item label="Correo Electrónico*"  className="select-item">
                        <Input type="email" value={payment.email} onChange={e=>setPayment({...payment, email: e.target.value})}/>
                    </Form.Item>
                    <Form.Item label="Cantidad a abonar*" className="select-item" >
                        <InputNumber type="number"   max={1000} min={100}  value={payment.amount} onChange={e=>setPayment({...payment, amount:e})}/>
                    </Form.Item>
                    <Form.Item label="Concepto" className="select-item" >
                        <Input placeholder="ej. anticipo" value={payment.concept} onChange={e=>setPayment({...payment, concept: e.target.value})}/>
                    </Form.Item>
                    
                </Form>
                <Popconfirm
                    title="¿Esta seguro de registrar el pago?"
                    onConfirm={onSubmit}
                    okText="Si"
                    cancelText="No"        
                >
                
                    <Button type="primary">Registrar Pago</Button>
                
                </Popconfirm>
          </>     
    );
}
 
export default RegisterPayment;