import React from 'react';
import  {Button, Input,InputNumber, notification, Popconfirm,Form}  from 'antd';
import {isEmpty, size} from 'lodash';
import { useState } from 'react';
import {validateEmail} from '../../../config/utils/Validations';

import UseAuth from '../../../Hooks/UseAuth';


import firebase from '../../../config/FireBase';
import 'firebase/firestore';



const AddAdminForm = () => {
    const {authState} = UseAuth();

    const [newAdmin, setNewAdmin] = useState({
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
        status: true,
        role: "admin"
    })
    const onSubmit =()=>{
        const {name, email, password, confirmPassword} = newAdmin;

        if(isEmpty(name)||isEmpty(email)||isEmpty(password)||isEmpty(confirmPassword)){
            return notification["error"]({
                message:    "Todos los campos son necesarios"
            });
        }else{
            if(size(password)<6){
                return notification["error"]({
                    message:    "La contraseña es muy corta"
                });
            }else{
                if(password!==confirmPassword){
                    return notification["error"]({
                        message: "Las contraseñas no coinciden"
                    });
                }else{
                    if(!validateEmail(email)){
                        return notification["error"]({
                                message: "El correo electrónico no es valido"
                        })
                    }else{
                        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(doc){
                            
                            firebase.firestore().collection('administradores').doc(doc.id).get().set({
                                name: useState.name, email: doc.email, status:true, role: "admin"
                            })
                            
                            setNewAdmin({
                                name: "",
                                email: "",
                                password: "",
                                confirmPassword: "",
                                status: true,
                                role: "admin"
                            });
                            return notification["success"]({
                                message: "El usuario se ha creado correctamente"
                            })
                        }).catch(()=>{
                            return notification["error"]({
                                message: "El correo electrónico ya existe"
                            })
                        })
                    }
                }
            }
        }
    }
    return (  
         
            <> 
                    <Form
                    
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                        
                    >
                    <Form.Item label="Nombre: "  className="select-item">
                        <Input  onChange={e=>setNewAdmin({...newAdmin, name: e.target.value})} value={newAdmin.name}/>
                    </Form.Item>
                    <Form.Item label="Correo Electrónico: "  className="select-item">
                        <Input type="email"  onChange={e=>setNewAdmin({...newAdmin, email: e.target.value})} value={newAdmin.email}/>
                    </Form.Item>
                    <Form.Item label="Contraseña: "  className="select-item">
                        <Input type="password"  onChange={e=>setNewAdmin({...newAdmin, password: e.target.value})} value={newAdmin.password}/>
                    </Form.Item>                    
                    <Form.Item label="Confirme contraseña: "  className="select-item">
                        <Input  onChange={e=>setNewAdmin({...newAdmin, confirmPassword: e.target.value})} value={newAdmin.confirmPassword}/>
                    </Form.Item>


                        
                    </Form>
                    <Popconfirm
                    title="¿Esta seguro de registrar al administrador?"
                    onConfirm={onSubmit}
                    okText="Si"
                    cancelText="No"        
                >
                
                    <Button type="primary">Registrar Administrador   </Button>
                
                </Popconfirm>
                   
            </>
        );
    
}
 
export default AddAdminForm;