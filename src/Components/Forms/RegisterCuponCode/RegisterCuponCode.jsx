import React from 'react';
import  {Button, Input,InputNumber, notification, Popconfirm,Form}  from 'antd';
import {isEmpty, size} from 'lodash';
import { useState } from 'react';

import UseAuth from '../../../Hooks/UseAuth';


import firebase from '../../../config/FireBase';
import 'firebase/firestore';



const RegisterCuponCode = () => {
    const {authState} = UseAuth();

    const [Cupon, setCupon] = useState({
        id:"",
        amount:50,
        createBy: authState.id,
        max: 1,
        status: true,
        used:0
        

    });
    const onSubmit =()=>{
        const {id, amount, max} = Cupon;

        if(isEmpty(id)||amount===null||max===null){
            return notification["error"]({
                message:    "Todos los campos son necesarios"
            });
        }else{
            if(size(id)<4){
                return notification["error"]({
                    message:    "El código es muy corto"
                });
            }else{
                firebase.firestore().collection('cupones').doc(id).get().then(function(doc){
                    if(doc.exists){
                        return notification["error"]({
                            message:    "El cupón ya ha sido registrado"
                        });
                    }else{
                        firebase.firestore().collection('cupones').doc(id).set(Cupon).then(()=>{
                            setCupon({
                                id:"",
                                amount:50,
                                createBy: authState.id,
                                max: 1,
                                status: true,
                                used:0});
                            return notification["success"]({
                                message:    "Se ha agregado el cupón exitosamente"
                            })
                        })
                    }
                })
            }
        }
    }
    return (  
         
            <>
                <h2>Agregar Cupón</h2>
                    <Form
                    
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                        
                    >
                        <Form.Item label="Código"  className="select-item">
                            <Input placeholder="ej. VIAJE21" onChange={e=>setCupon({...Cupon, id: e.target.value})}/>
                        </Form.Item>
                        <Form.Item label="descuento " className="select-item" >
                            <InputNumber  placeholder="ej. 500"  max={1000} min={50} value={Cupon.amount}   
                                onChange={e=>setCupon({...Cupon, amount: e})}/>
                        </Form.Item>
                        <Form.Item label="cantidad de cupones " className="select-item" >
                            <InputNumber placeholder = "ej. 5"  max={20} min={1} value={Cupon.max}
                                onChange={e=>setCupon({...Cupon, max: e})}/>
                        </Form.Item> 
                    </Form>
                    <Popconfirm
                    title="¿Esta seguro de registrar el pago?"
                    onConfirm={onSubmit}
                    okText="Si"
                    cancelText="No"        
                >
                
                    <Button type="primary">Registrar Pago</Button>
                
                </Popconfirm>
                   
            </>
        );
    
}
 
export default RegisterCuponCode;