import React  from 'react';
import {Card, Form, Button, Input, DatePicker, Radio, Checkbox, notification} from 'antd';
import { useState } from 'react';
import {isEmpty, size} from 'lodash';
import "./RegisterForm.scss";
//server
import firebase from '../../../config/FireBase';
import'firebase/auth';
import'firebase/firestore';

import UseQuotation from '../../../Hooks/UseQuotation';
import UseAuth from '../../../Hooks/UseAuth';
import {validateEmail} from '../../../config/utils/Validations';

const RegisterForm = (props) => {
    const {quotationState} = UseQuotation();
    const {auth} = UseAuth();

    const [errorName, setErrorName] = useState(false);
    const [errorLastNameF, setErrorLastNameF] = useState(false);
    const [errorLastNameM, setErrorLastNameM] = useState(false);
    const [errorBirth, setErrorBirth] = useState(false);
    const [errorEmail, setErrorEmail] = useState(false);
    const [errorPassword, setErrorPassword] = useState(false);
    const [errorPasswordConfirm, setErrorPasswordConfirm] = useState(false);

    const [userData, setUserData] = useState({
        name:"",
        lastNameF:"",
        lastNameM:"",
        birth:"",
        birthYear:null,
        email:"",
        password:"",
        passwordConfirm:"",
        room:false,
        policy:false

    });
    const onChange = (e, key) =>{
        
        setUserData({
            ...userData,
            [key]: e.target.value
        });

        
    }  
    const changeDate =(e)=>{
        if(e){
            setUserData({
                ...userData,
                birth: `${e._d.getDate()}/${e._d.getMonth()+1}/${e._d.getFullYear()}`,
                birthYear: e._d.getFullYear()
            })
        }
    }
    const onsubmit = ()=>{
        
       //destructuring
       const {name, lastNameF, lastNameM,
            birth, birthYear, email, password, passwordConfirm,
            room,roomCode,  policy} = userData;

        //validar espacios vacios
        if(isEmpty(name)){setErrorName(true)}else{setErrorName(false)}
        if(isEmpty(lastNameF)){setErrorLastNameF(true)}else{setErrorLastNameF(false)}
        if(isEmpty(lastNameM)){setErrorLastNameM(true)}else{setErrorLastNameM(false)}
        if(isEmpty(birth)){setErrorBirth(true)}else{setErrorBirth(false)}
        if(isEmpty(email)){setErrorEmail(true)}else{setErrorEmail(false)}
        if(isEmpty(password)){setErrorPassword(true)}else{setErrorPassword(false)}
        if(isEmpty(passwordConfirm)){setErrorPasswordConfirm(true)}else{setErrorPasswordConfirm(false)}
     

        if(isEmpty(name)||//retornar mensaje de error por estar vacío
        isEmpty(lastNameF)||
        isEmpty(lastNameM)||
        isEmpty(birth)||
        isEmpty(email)||
        isEmpty(password)||
        isEmpty(passwordConfirm)){
            return notification["error"]({
                message: "Todos los campos son obligatorios"
            });
        }

        if(!policy){
            return notification["error"]({
                message: "Es necesario que Acepte los terminos y condiciones"
            });
        }
        
      
        


        //validar fecha de nacimiento
        if(birthYear>2003){
            setErrorBirth(true);
            return notification["error"]({
                message: "Lo sentimos pero usted tiene que ser mayor de edad"
            });
        }else{
            setErrorBirth(false);
        }
        //validar email
        if(!validateEmail(email)){
            setErrorEmail(true);
            return notification["error"]({
                message: "correo Invalido"
            });
        }else{
            setErrorEmail(false);
        }

        //validar password
        if(size(password)<=7){
            setErrorPassword(true);
            setErrorPasswordConfirm(true);
            return notification["error"]({
                message: "Contraseña muy corta"
            });
        }else{
            if(passwordConfirm!==password){
                setErrorPassword(true);
            setErrorPasswordConfirm(true);
            return notification["error"]({
                message: "Las contraseñas no coinciden"
            });
            }else{
                setErrorPassword(false);
                setErrorPasswordConfirm(false);
            }
        } 


        //------->succes
        
       
            let id;
           firebase.auth().createUserWithEmailAndPassword(email,password).then(response=>{
                setErrorEmail(false);
               id=response.user.uid;
            }).then(()=>{
                firebase.firestore().collection('usuarios').doc(id).set({
                    name: `${name} ${lastNameF} ${lastNameM}`,
                    birth: birth,
                    email: email,
                    photoURL: null,
                    room:quotationState.room,
                    boardTrip: quotationState.boardtrip,
                    cuponCode: quotationState.cuponCode,
                    descount: quotationState.descount,
                    drinks: quotationState.drinks,
                    party: quotationState.party,
                    price: quotationState.price,
                    priceParty: quotationState.price,
                    status: true,
                    role:'user'
                }
        ).then((responseF)=>{ 
                    
                    return auth();
    
                }).catch((err)=>{
                    return notification["error"]({
                        message: "Error al guardar en la base de datos"
                    });
                })}
            ).catch((err)=>{
               setErrorEmail(true);
               return notification["error"]({
                message: "El correo ya ha sido registrado anteriormente"
        });
           })
            
        
        
    }




    return(
    <div className="register-form">

    <Card className="register-form-card">
        <h1>Registro</h1>
        <Form
        labelCol={{
            span: 12,
        }}
        wrapperCol={{
            span: 14,
        }}
        
       >
           <Form.Item  className="select-item" label="Nombre(s)"
             validateStatus={errorName ?"error" :""}>
                 <Input value={userData.name} placeholder="Nombre(s)"  onChange={(e)=>onChange(e,"name")}/>
           </Form.Item>
           <Form.Item  className="select-item" label="Apellido Paterno "
             validateStatus={errorLastNameF ?"error" :""}>
                 <Input value={userData.lastNameF} placeholder="Apellido Paterno " onChange={(e)=>onChange(e,"lastNameF")}/>
             </Form.Item>
            
            <Form.Item   className="select-item" label="Apellido Materno"
             validateStatus={errorLastNameM ?"error" :""}>
                <Input value={userData.lastNameM} placeholder="Apellido Materno" onChange={(e)=>onChange(e,"lastNameM")}/>
            </Form.Item>

            <Form.Item label="Fecha de nacimiento" className="select-item"
             validateStatus={errorBirth ?"error" :""}>   
                 <DatePicker  picker="date" onChange={(e)=>changeDate(e)}/>
            </Form.Item> 

            <Form.Item label="Correo Electrónico" className="select-item"
             validateStatus={errorEmail ?"error" :""}>  
                <Input value={userData.email} placeholder="correo electronico" type="email" onChange={(e)=>onChange(e,"email")}/>
            </Form.Item>

            <Form.Item label="Contraseña" className="select-item" 
             validateStatus={errorPassword ?"error" :""}>  
                <Input value={userData.password} type="password" placeholder="contraseña" onChange={(e)=>onChange(e,"password")}/>
            </Form.Item>

            <Form.Item label="Confirma Contraseña" className="select-item"
             validateStatus={errorPasswordConfirm ?"error" :""}>  
                <Input value={userData.passwordConfirm} type="password" placeholder="Repetir contraseña" onChange={(e)=>onChange(e,"passwordConfirm")}/>
            </Form.Item>

            <Form.Item>
                <br/>
                <Checkbox onChange={e=>setUserData({...userData, policy: e.target.checked})}>He leído los <a href="/" >Términos y Condiciones</a></Checkbox>
                <br/><br/>
                <Button type="primary" onClick={onsubmit}>Registrarme</Button>
            </Form.Item>
        </Form>
    </Card>
    </div>
    );
}
 
export default RegisterForm;