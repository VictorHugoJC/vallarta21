import React, {useState} from 'react'
import {size, isEmpty} from 'lodash';
import {Form, Input, Button, notification} from 'antd';
import { MailOutlined, LockOutlined} from '@ant-design/icons';

import firebase  from '../../../config/FireBase';
import'firebase/auth';
import './LoginForm.scss';

import {validateEmail} from '../../../config/utils/Validations';
import UseAuth from '../../../Hooks/UseAuth';



const Login = () => {
  const [user, setUser] = useState({
    email:"",
    password: ""
  });
  const {email, password} = user;
  const {auth} = UseAuth();

  const onSubmit= () =>{
  
    if(
      isEmpty(email)||
      isEmpty(password)
  ){
    notification["error"]({
      message: 'Todos los campos son necesarios'
    });
  }else{        
      const emailValidate = validateEmail(email);
      if(!emailValidate){
          notification["error"]({
            message: 'Email no valido'
          });
      }else {
          if(size(password)<=6){
            notification["error"]({
              message: 'Contraseña no valida'
            });
          }else{
              //login
              firebase.auth().signInWithEmailAndPassword(email, password).then(response=>{
               
                auth();
                               
              }).catch(err=>{
                notification["error"]({
                  message: "Contraseña o correo incorrectos"
                })
              });
        }
    }
  }



}



 
    return (
      <Form className= "login-form">
      <Form.Item>-
       <Input
       prefix = {<MailOutlined style={{color: "rgba(0,0,0,.25)" }}/>}
       type = "email"
       value = {email}
       name = 'email'
       placeholder = 'correo electrónico'
       className = "login-form__input" 
      onChange={e=>setUser({...user, email:e.target.value})}
       />
   </Form.Item>
   <Form.Item>
       <Input
       prefix = {<LockOutlined style={{color: "rgba(0,0,0,.25)" }}/>}
       type = "password"
       value = {password}
       name = 'password'
       placeholder = 'contraseña'
       className = "login-form__input" 
      onChange={e=>setUser({...user, password:e.target.value})}
       />
   </Form.Item>
   <Form.Item>
       <Button htmlType="submit" className="login-form__button" onClick={()=>onSubmit()}>
         Iniciar Sesión
       </Button>
   </Form.Item>
 </Form>
      );
 
}
 
export default Login; 