import React,{useState, useEffect} from 'react';
import './Quotation.scss';
import Modal from '../../Modal';
import InfoTrip from './InfroTrip';
import {Form,
    Card,
    Input,
    Button,
    Radio,
    Select,
    InputNumber,
    notification
} from 'antd';
import UseQuotation from '../../../Hooks/UseQuotation';

import firebase from '../../../config/FireBase';
import 'firebase/firestore';


const UserInfo = () => {
    

    useEffect(()=>{
        firebase.firestore().collection('config').doc('ZJJa8vl6cdhyUiGQwROt').get().then(doc=>{
            setPriceTrip(
                {
                    triple: doc.data().tripleRoomPrice,
                    doble: doc.data().dobleRoomPrice,
                    antro: doc.data().antro,
                    barra: doc.data().barra,
                    bote: doc.data().bote,
                }
            )
        })
    },[])
    const onChange = (e, key) =>{
        
        setQuotation({
            ...quotation,
            [key]: e
        });

        
    }   
    const onClick=(e)=>{
        //validación
        
        const {room, party, drinks, boardtrip, cuponCode}=quotation;
        let descount=0;
        let price=0;
        let priceP=null;

        if(room===""||party===null){    
            
            if(room===""&&party===null){
                setErrorParty(true);
                setErrorRoom(true);               
            }else{
                if(party===null){
                    setErrorParty(true);     
                    setErrorRoom(false);   
                }else{
                    setErrorRoom(true);
                    setErrorParty(false);     
                }    
            }
            return (notification.error({
                message:"Por favor llena los campos"
            }));

        }
        setErrorRoom(false);   
        setErrorParty(false);  
        const getDescont=(data)=>{
            descount=data;
        }
        //validar cupón
        if(cuponCode!==""){
            
            firebase.firestore().collection('cupones').doc(cuponCode).get().then(function(doc){
                
                
                if(doc.exists){
                        if(doc.data().max>doc.data().used&&doc.data().status===true){
                            if(doc.data().amount!==0){
                                
                                price = price - doc.data().amount;
                                setQuotation({
                                    ...quotation,
                                    price: price,
                                    descount: doc.data().amount
                                })
                            }
                            descount = doc.data().amount;
                            getDescont(doc.data());
                            setErrorCupon(false);
                        }else{
                            
                            setErrorCupon(true);
                            notification["error"]({
                                message: "Este código ya no es válido"
                            })
                        }
                }else{

                     setErrorCupon(true);
                }
            })
            
        }else{
            setQuotation({...quotation,
                cuponCode: null});
                setErrorCupon(false)
        }
        
        
        if(room==="triple"){
                price = priceTrip.triple;
        }else{
                price = priceTrip.doble;
        }
        if(party){
            priceP=(party-drinks)*priceTrip.antro;
            priceP+=drinks*priceTrip.barra;
            price+=priceP;
        }
        if(boardtrip){
            price+=priceTrip.bote;
        }

        if(cuponCode){
            
            price-=descount;
        }

        setQuotation({
            ...quotation,
            price:price,
            descount: descount,
            priceParty:priceP
        });
        setChange(true)
      
    if(errorCupon===false&&change===true){
        setisVisible(true);
}          
    } 
    return (
      <>
      <div className="quotation">
      <Card className="quotation-card">
        <Form
            labelCol={{
                span: 6,
            }}
            wrapperCol={{
                span: 14,
            }}
           
            
            >
            <h2>Llena los campos solicitados</h2>
            <Form.Item label="*Tipo de Habitación" className="select-item"
             validateStatus={errorRoom ?"error" :""}
            >
                <Select onChange={(e)=>onChange(e,"room")}  placeholder="-Seleccionar-">
                <Select.Option value="doble">Doble</Select.Option>
                <Select.Option value="triple">Triple</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item label="*Noches de Antro" className="select-item" name="party" 
                validateStatus={errorParty ?"error" :""}
            >
                <Select className="quotation-input" name="party" onChange={(e)=>onChange(e,"party")} placeholder="-Seleccionar-">
                <Select.Option value={0}> Ninguna</Select.Option>
                <Select.Option value={1}>Una Noche</Select.Option>
                <Select.Option value={2}>Dos Noches</Select.Option>
                <Select.Option value={3}>Tres Noches</Select.Option>
                </Select>
            </Form.Item>
            {   
                quotation.party
                ?(<Form.Item label="*Barras Libres: ">
                <InputNumber min={0} max={quotation.party} name="drinks" value={quotation.drinks} onChange={(e)=>onChange(e,"drinks")}/>
                </Form.Item>)
                :null 
            }
            <Form.Item  label="*Paseo en Barco" className="select-item">
                <Radio.Group onChange={(e)=>onChange(e.target.value,"boardtrip")} defaultValue={quotation.boardtrip}> 
                    <Radio value={true}>Sí</Radio>
                    <Radio value={false}>No</Radio>
                </Radio.Group>
            </Form.Item >
            <Form.Item className="cupon">
                <a onClick={(e)=>onChange(true,"cupon")} >Tengo Cupón</a>
            </Form.Item>

           {    quotation.cupon
                ?(
                    <Form.Item label="Codigo:" className="cupon cupon-code "
                    hasFeedback={errorCupon}
                         validateStatus={errorCupon ?"error" :""}
                         help={errorCupon ?"Cupón no válido" :""}
                    >
                        <Input onChange={e=>onChange(e.target.value,"cuponCode")} placeholder="Escriba su cupón"
                        />
                    </Form.Item>)
                :null
           }
            <Form.Item>
                <Button type="primary" className="btn-quotation" onClick={e=>onClick(e)}>Cotizar</Button>
            </Form.Item>
            
            </Form>
      </Card>
      </div>

      <Modal 
        isVisible={isVisible} 
        setIsVisible={setisVisible}
        width={'80%'}>
           <InfoTrip quotation={quotation} button={true} setQuotationState={setQuotationState}/>
      </Modal>

      </>
    );
  };
export default Quotation;