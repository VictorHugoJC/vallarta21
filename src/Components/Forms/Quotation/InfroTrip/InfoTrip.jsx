import React,{Fragment} from 'react';
import Register from '../../../../pages/Register';
import {Redirect, Route} from 'react-router-dom';
import {
    Card,
    Row,
    Col,
    Image,
    Button
} from 'antd';
import {CheckOutlined, CloseOutlined, StarOutlined} from '@ant-design/icons';
import './InfoTrip.scss';
import { useState } from 'react';
import UseQuotation from '../../../../Hooks/UseQuotation';


const InfoTrip = (props) => { 
     const {quotation, button, setQuotationState} = props;
    const [payment, setPayment] = useState(false);
    const onClick =(e)=>{  
        setQuotationState(quotation)
        setPayment(true);

    
    }
    
   
    return ( 
        <Fragment>
            <Row>
                
                <Col lg={24}>
                    <Row>
                        <Col md={12}>
                        <Card bordered={false}>
                        <Image 
                            src='https://cdn.galaxy.tf/thumb/sizeW400/unit-media/tc-default/uploads/images/room_photo/001/589/303/resort-room-two-double-beds-01-standard.jpg'
                            
                        />
                        </Card>
                        </Col>
                        <Col md={12}>
                        <Card bordered={false} className="content">
                            <h1 className="info-trip-room">Habitación {quotation.room}</h1>
                            <h2>${quotation.price}.00 Mx</h2>
                            {quotation.descount ?(<h3 style={{textDecoration:'line-through'}}>${quotation.price + quotation.descount}.00 Mx</h3>) :null}
                            {quotation.cuponCode    ?(<p><CheckOutlined /> Cupón <strong>{quotation.cuponCode}</strong></p>) :null}
                            {quotation.boardtrip ?(<p><CheckOutlined /> Paseo en Barco         ($500.00)</p>) :<p><CloseOutlined /> Paseo en Barco</p>}
                            {quotation.party ?(<p><StarOutlined />{quotation.party} noches de Antro, {quotation.drinks} barras libre (${quotation.priceParty})</p>) 
                                :<p><CloseOutlined /> Antros</p>}
                            {/*<p style={{color:"red"}}>Quedan <strong>5 Días</strong> para que la tarifa aumente</p>*/}
                            {button 
                                    ?(<Button onClick={e=>onClick(e)} htmlType="submit" type="primary">Apartar</Button>)
                                    :null}
                        </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
            {payment 
                ?(
                <><Route path="/register" component={Register} />
                 <Redirect to="/register" /></>)
                :null}
        </Fragment>
     );
}
 
export default InfoTrip;