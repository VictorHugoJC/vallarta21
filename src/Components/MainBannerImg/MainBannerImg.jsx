import React from 'react';
import {Row, Col, Button} from 'antd';
import {Link} from 'react-router-dom';
import './MainBannerImg.scss';


const MainBannerImg = ({title, content, linkTo}) => {
    return (  
        <div className="home-main-banner">
        <div className="home-main-banner__dark"/>

        <Row>
            <Col lg={4}/>
            <Col lg={16}>
                <h2>
                {title}
                </h2>
                <h3>
                    {content}
                </h3>
                <Link to={linkTo}><Button type="primary" className="home-main-banner-btn">Ver Paquete</Button></Link>
            </Col>
            <Col lg={4}/>
        </Row>
    </div>
    );
}
 
export default MainBannerImg;