import React from 'react'
import {Link, withRouter} from 'react-router-dom';//withRouter para menu seleccionado (export withRouter y se obtiene la location)
import {Layout, Menu} from 'antd';
import { HomeOutlined,UserOutlined,DollarCircleOutlined}from '@ant-design/icons';
import './MenuSider.scss';
/*
import firebase from '../../../config/FireBase';
import 'firebase/firestore';*/
 
import UseAuth from '../../../Hooks/UseAuth';



const MenuSider = (props) => {
    const {menuColapsed, location} = props;
    const {Sider} = Layout;
    const id = UseAuth().authState.id;
    
    


    return (  
        <Sider className = "admin-sider" collapsed = {menuColapsed}>
            <Menu theme="dark" mode= "inline" defaultSelectedKeys= {[location.pathname]}>
            <Menu.Item key ="/user">
                    <Link to={"/user"}>
                        <HomeOutlined/>
                        <span className = "nav-text">Home</span>
                    </Link>
            </Menu.Item>
            <Menu.Item key="/tickets">
                <Link to={"/user/recibos"}>
                    <DollarCircleOutlined />
                    <span className="nav-text">Recibos</span>
                </Link>
            </Menu.Item>
            <Menu.Item key ="/info">
                <Link to={"/user/info"}>
                    <UserOutlined />
                    <span className = "nav-text">Info</span>
                </Link>
            </Menu.Item>
            </Menu>
        </Sider>
    );
}
 
export default withRouter(MenuSider)
;