import React,{useState} from 'react'
import './NewsLetter.scss';
import {Form,Input,notification, Button} from 'antd';
import {UserOutlined} from '@ant-design/icons';
import firebase from '../../../config/FireBase';

const NewsLetter = () => {
    const [emailNewsletter, setEmailNewsletter] = useState("");
    
    const onSubmit=async e=>{
        e.preventDefault();

        //Validacion de email

        // eslint-disable-next-line
        if(emailNewsletter.length<=4|| !/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/.exec(emailNewsletter)){
            return notification["error"]({
                message:"El correo no es válido"
            });
        }else{
            firebase.firestore().collection('newsLetter').doc(emailNewsletter).get().then(doc=>{
                if(doc.exists){
                    return notification["error"]({
                        message: "Ya se ha registrado previamente"
                    })
                }else{
                    firebase.firestore().collection('newsLetter').doc(emailNewsletter).set({
                        active: false
                    }).then(()=>{
                        setEmailNewsletter("");
                        return notification["success"]({
                            message: "Se ha registrado su email correctamente"
                        })
                    })
                }
            })
        }
       
        
        


    }

    return (  
        <div className="newsletter">
                <h3>Newsletter</h3>
                <Form onSubmit={onSubmit}>
                    <Form.Item>
                        <Input
                            prefix={<UserOutlined />}
                            placeholder="Correo Electrónico"
                            value={emailNewsletter}
                            onChange={(e)=>setEmailNewsletter(e.target.value)}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={onSubmit} className="btn-news-letter">¡Me suscribo!</Button>
                    </Form.Item>
                </Form>
        </div>
    );
}
 
export default NewsLetter;