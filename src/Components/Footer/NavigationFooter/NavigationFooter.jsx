import React, { Fragment } from 'react';
import './NavigationFooter.scss';
import {Row, Col} from 'antd'
import {Link} from 'react-router-dom';
import {StarOutlined , LaptopOutlined, WhatsAppOutlined} from '@ant-design/icons';
const NavigationFooter = () => {
    return (  
        <Fragment>
        <Row className="navigation-footer">
            <Col md={24}>
                <h3>Links Rápidos: </h3>
            </Col>
        </Row>
        <Row>
            <Col md={12}>
                <RenderListLeft/>
            </Col>
            <Col md={12}>
                <RenderRight/>
            </Col>
        </Row>
        </Fragment>
    );
}
 
export default NavigationFooter;

export function RenderListLeft(){
    return(
        <ul>
        <li>
            <Link to="/vacation-plan">
            <StarOutlined /> Paquete
            </Link>
        </li>
        <li>
        <Link to="/process">
            <LaptopOutlined /> proceso
        </Link>
        </li>
    </ul>
    );
}

export function RenderRight(){
    return(
    <ul>
        <li>
         {/*eslint-disable-next-line */}
            <a href="https://api.whatsapp.com/send?phone=524448500377" target="_blank" rel="noopener referrer">
                <WhatsAppOutlined /> WhatsApp
            </a>
        </li>
    </ul>
    )
}