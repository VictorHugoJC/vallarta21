import React from 'react';
import {Layout, Col,Row} from 'antd';
import './Footer.scss';
import Info from './Info';
import NavigationFooter from './NavigationFooter';
import NewsLetter from './NewsLetter';


const Footer = () => {
    const {Footer} = Layout;
    return ( 
        <Footer className="footer">
            <Row>
                <Col md={4}/>
                <Col md={16}>
                    <Row>
                    <Col md={8}>
                        <Info/>
                    </Col>
                    <Col md={8}>
                    <NavigationFooter/>
                    </Col>
                    <Col md={8}>
                        <NewsLetter/>
                    </Col>
                    </Row>
                    <Row className="footer__copyright">
                        <Col md={12}>2020 Todos los derechos son Reservados para COVITRIP21 </Col>
                        <Col md={12}></Col>
                    </Row>
                </Col>
                <Col md={4}/>
            </Row>
        </Footer>
    );
}
 
export default Footer;