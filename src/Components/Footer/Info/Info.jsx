import React from 'react'
import './Info.scss';
import Logo from '../../../assets/img/Logo-covitrip21.png';
const Info = () => {
    return (  
        <div className="my-info">
            <img src={Logo} alt=""/>
            <h4>
                Sé parte de esta aventura,
                estamos seguros que será un viaje
                divertido, inolvidable y una experiencia
                única
            </h4>
           
        </div>
    );
}
 
export default Info;