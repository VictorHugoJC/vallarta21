import React, { useState } from 'react'
import { Switch, Button } from 'antd';
import './ListUser.scss';
import CardUser from './CardUser';
import Modal from '../../Modal';
import RegisterForm from '../../Forms/RegisterForm';
import {UserAddOutlined} from '@ant-design/icons';
import AddAdminForm from '../../Forms/AddAdminForm';


const ListUser = (props) => {
    const {usersInactive, usersActive, setReloadUsers, superAdmin, useModal} = props;
    const [viewUserActive, setviewUserActive] = useState(true);
    const [isVisible, setIsVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState("");
    const [modalContent, setModalContent] = useState("");
    
    const newUser=(e)=>{//Cuando da clic en el botón de agregar
        e.preventDefault();
        setModalTitle("Nuevo user");
        setModalContent(<AddAdminForm/>);
        setIsVisible(true);
    }
   
     
    return (  
        <div className="list-user">
            <div className = "list-user__header">
                <div className ="list-user__header-switch">
                    <Switch
                    defaultChecked
                    onChange = {() =>setviewUserActive(!viewUserActive)}/>
                    <span className="list-user__header-span">
                        {
                            superAdmin
                            ? viewUserActive ? "Administradores Activos" : "Administradores Inactivos"
                            : viewUserActive ? "usuarios Activos" : "Usuarios Inactivos"
                        }
                        
                    </span>
                </div>
                
            </div>
           { viewUserActive
           ? (<CardUser users={usersActive} setReloadUsers={setReloadUsers} useModal={useModal}/> )
           : (<CardUser users={usersInactive} setReloadUsers={setReloadUsers}/> )

           }
           
           <Modal
            title = {modalTitle}
            isVisible = {isVisible}
            setIsVisible = {setIsVisible}
        >
            {modalContent}        
        </Modal>
        </div>
    );
}

 
export default ListUser;