import React, { useState } from 'react';
import './ListUser.scss';
import {Button, notification} from 'antd';
import noAvatar from '../../../assets/img/avatar-default.jpg';
import {List} from 'antd'
import Avatar from 'antd/lib/avatar/avatar';
import {Link} from 'react-router-dom';
import { StopOutlined,CheckOutlined} from '@ant-design/icons';
import Modal from '../../Modal';
import UserInfo from '../../UserInfo';

import firebase from '../../../config/FireBase';
import 'firebase/firestore';

import PaymentInfo from '../../PaymentInfo';

const CardUser = ({users, useModal}) => {

    const [isVisible, setisVisible] = useState(false);
    const [content, setContent] = useState();

    
    const openModal=(id)=>{
        
        setContent(
            <>
            <UserInfo id={id}/>
            <PaymentInfo id={id}/>
            </>
            );
        setisVisible(true);
    }

    const userEdit=(user)=>{
        
        if(user.role==="user"){
            firebase.firestore().collection('usuarios').doc(user.id).set({
                status: !user.status,
                birth: user.birth,
                boardTrip: user.boardTrip,
                cuponCode: user.cuponCode,
                descount: user.descount,
                drinks: user.drinks,
                email: user.email,
                name: user.name,
                party: user.party,
                price: user.price,
                priceParty: user.priceParty,
                role: user.role,
                roomCode: user.roomCode,
            }).then(()=>{
                return notification["success"]({
                    message: "Se ha actualizado el estado"
                })
            })
        }else{
            
                firebase.firestore().collection('administradores').doc(user.id).set({
                    status: !user.status,
                    email: user.email,
                    name: user.name,
                    role: user.role,
                    
                }).then(()=>{
                    return notification["success"]({
                        message: "Se ha actualizado el estado"
                    })
                })
           
        }
}
    return (  
        <>
        <List
            className= "user-active"
            itemLayout="horizontal"
            dataSource={users}
            
            renderItem={user => (
                <List.Item
                actions={[
                    user.status
                    ?(<Button 
                        onClick={()=>userEdit(user)}
                        type= "danger"
                     >  
                      <StopOutlined />
                      </Button>)
                    :(<Button onClick={()=>userEdit(user)}
                        type= "primary"
                    >  
                        <CheckOutlined />
                      </Button>)
                      
    
                ]}
                >
                    <List.Item.Meta
                        avatar={<Avatar src={noAvatar} />}
                        title={
                            useModal
                                ?<Link to="/admin/users" onClick={()=>openModal(user.id)}>{user.name}</Link>
                                :user.name
                        }
                        description={user.email}
                    />
                </List.Item>
            )}
        />
        <Modal isVisible={isVisible} setIsVisible={setisVisible} >{content}</Modal>
        </>
    );
}
 
export default CardUser;


