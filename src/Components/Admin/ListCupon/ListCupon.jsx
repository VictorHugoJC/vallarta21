import React, { useState } from 'react'
import { Switch, Button } from 'antd';
import './ListUser.scss';
import CardUser from './CardList';
import Modal from '../../Modal';
import RegisterCuponCode from '../../Forms/RegisterCuponCode';
import {TagOutlined} from '@ant-design/icons';



const ListUser = (props) => {
    const {cuponsInactive, cuponsActive, setReloadCupons} = props;
    const [viewUserActive, setviewUserActive] = useState(true);
    const [isVisible, setIsVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState("");
    const [modalContent, setModalContent] = useState("");
    
    const newUser=(e)=>{//Cuando da clic en el botón de agregar
        e.preventDefault();
        setModalTitle("Nuevo Cupón");
        setModalContent(<RegisterCuponCode setReloadUsers={setReloadCupons} setIsVisible={setIsVisible}/>);
        setIsVisible(true);
    }
   
     
    return (  
        <div className="list-user">
            <div className = "list-user__header">
                <div className ="list-user__header-switch">
                    <Switch
                    defaultChecked
                    onChange = {() =>setviewUserActive(!viewUserActive)}/>
                    <span className="list-user__header-span">
                        {
                            
                             viewUserActive ? "Cupones Activos" : "Cupones Inactivos"
                           
                        }
                        
                    </span>
                </div>
                
                    
                    <Button type="primary" onClick={newUser}><TagOutlined />Nuevo Cupón</Button>
                    
            </div>
           { viewUserActive
           ? (<CardUser users={cuponsActive} setReloadUsers={setReloadCupons}/> )
           : (<CardUser users={cuponsInactive} setReloadUsers={setReloadCupons}/> )

           }
           
           <Modal
            title = {modalTitle}
            isVisible = {isVisible}
            setIsVisible = {setIsVisible}
        >
            {modalContent}        
        </Modal>
        </div>
    );
}

 
export default ListUser;