import React from 'react';
import './ListUser.scss';
import {Button, notification} from 'antd';
import {List} from 'antd'
import { StopOutlined,CheckOutlined} from '@ant-design/icons';
import firebase from '../../../config/FireBase';
import 'firebase/firestore';


const CardUser = ({users}) => {
    const statusUser=user=>{
        firebase.firestore().collection('cupones').doc(user.id).set({
            amount: user.amount,
            createBy: user.createBy,
            status: !user.status,
            used: user.used,
            max: user.max
        }).then(()=>{
            return notification["success"]({
                message: "Se ha modificado el estado del cupón"
            });
        })
    }
    return (  
        <List
            className= "user-active"
            itemLayout="horizontal"
            dataSource={users}
            
            renderItem={user => (
                <List.Item
                actions={[
                    
                      user.status
                      ?(<Button 
                            type= "danger"
                            onClick={()=>statusUser(user)}
                        >  
                            <StopOutlined />
                        </Button>)
                        :<Button 
                            onClick={()=>statusUser(user)}
                            type= "primary"
                        >  
                            <CheckOutlined />
                        </Button>
                ]}
                >
                    <List.Item.Meta
                        
                        title={`${user.id}  $${user.amount}MXN`}
                        description={`cupones:  ${user.max} |   usados:    ${user.used}`}
                    />
                </List.Item>
            )}
        />
    );
}
 
export default CardUser;


