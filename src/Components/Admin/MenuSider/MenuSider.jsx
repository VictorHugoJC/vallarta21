import React, { useState } from 'react'
import {Link, withRouter} from 'react-router-dom';//withRouter para menu seleccionado (export withRouter y se obtiene la location)
import {Layout, Menu} from 'antd';
import {TagOutlined, HomeOutlined, SolutionOutlined,
    SettingOutlined,
     TeamOutlined, CrownOutlined, DollarOutlined}from '@ant-design/icons';
import './MenuSider.scss';

import firebase from '../../../config/FireBase';
import 'firebase/firestore';
import UseAuth from '../../../Hooks/UseAuth';



const MenuSider = (props) => {
    const {menuColapsed, location} = props;
    const {Sider} = Layout;
    const id = UseAuth().authState.id;
    const [adminRole, setadminRole] = useState("");

    firebase.firestore().collection('administradores').doc(id).get().then(function(doc){
        setadminRole(doc.data().role);
    })


    return (  
        <Sider className = "admin-sider" collapsed = {menuColapsed}>
            <Menu theme="dark" mode= "inline" defaultSelectedKeys= {[location.pathname]}>
                <Menu.Item key ="/admin">
                    <Link to={"/admin/"}>
                        <HomeOutlined/>
                        <span className = "nav-text">Home</span>
                    </Link>
                </Menu.Item>
                
                <Menu.Item key ="/admin/payments">
                    <Link to={"/admin/payments"}>
                    <DollarOutlined />
                        <span className = "nav-text"> recibos </span>
                    </Link>
                </Menu.Item>
                <Menu.Item key ="/admin/cupons">
                    <Link to={"/admin/cupons"}>
                    <TagOutlined />
                        <span className = "nav-text"> Cupones </span>
                    </Link>
                </Menu.Item>
                <Menu.Item key ="/admin/rooms">
                    <Link to={"/admin/rooms"}>
                            <SolutionOutlined />
                        <span className = "nav-text"> Habitaciones </span>
                    </Link>
                </Menu.Item>
                <Menu.Item key ="/admin/users">
                    <Link to={"/admin/users"}>
                    <TeamOutlined />
                        <span className = "nav-text"> usuarios </span>
                    </Link>
                </Menu.Item>
               { adminRole==="superAdmin" ?<Menu.Item key ="/admin/admin-profiles">
                    <Link to={"/admin/admin-profiles"}>
                    <CrownOutlined />
                        <span className = "nav-text"> Administradores </span>
                    </Link>
                </Menu.Item>
                :null}
                {adminRole==="superAdmin"
                    ?<Menu.Item  key ="/admin/configuration">
                    <Link to={"/admin/configuration"}>
                        <SettingOutlined />
                        <span className = "nav-text"> Configuración </span>
                    </Link>
                </Menu.Item>
                :null}
            </Menu>
        </Sider>
    );
}
 
export default withRouter(MenuSider)
;