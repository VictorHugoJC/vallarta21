import React from 'react';
import './MenuTop.scss';
import {Button} from 'antd';
import { MenuUnfoldOutlined, LoginOutlined, MenuFoldOutlined } from '@ant-design/icons';
import logo from '../../../assets/img/Logo-covitrip21.png';
import firebase from '../../../config/FireBase';
import'firebase/auth';



const MenuTop = ({setmenuColapsed, menuColapsed}) => {
    const LoginOut = () =>{
        firebase.auth().signOut().then(function() {
                window.location.href ='/login';
          });

    }
    
    return (  
        <div className= "menu-top">
            <div className = "menu-top__left">
                <img className="menu-top__left-logo"
                src= {logo}
                alt="Victor Hugo Jimenez"
                />
                <Button type= "link" onClick = {() => setmenuColapsed(!menuColapsed)} >
                    {menuColapsed ? <MenuUnfoldOutlined/> : <MenuFoldOutlined />}
                   
                </Button>
            </div>
            <div className = "menu-top_right">
              <Button type= "link"  onClick = {LoginOut} ><LoginOutlined /></Button>  
            </div>
        </div>
    );
}
 
export default MenuTop;