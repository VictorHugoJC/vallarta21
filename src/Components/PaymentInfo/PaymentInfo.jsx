import React, { useEffect, useState } from 'react';
import {Card, Table, Spin} from 'antd';
import './PaymentUser.scss';
import firebase from '../../config/FireBase';

const PaymentInfo = ({id, heigh, width}) => {
    
    const [dataSource, setDataSource] = useState([]);
    const [total, setTotal] = useState(0);
    let paid=0;
    

    useEffect(()=>{
        if(id){
            
            firebase.firestore().collection('usuarios').doc(id).get().then(function(doc){
                if(doc.exists){
                    setTotal(doc.data().price);
                }
            });

            firebase.firestore().collection('pagos').where('user',"==",id).onSnapshot(function(snapshot){
                const data= snapshot.docs.map(doc=>{
                    const payment = doc.data();
                    payment.key = doc.id;
                    payment.amountStr = `$${doc.data().amount}.00`
                    return payment;
                });
                setDataSource(data);
                
            })
        }
    },[id])


    if(dataSource){
        for(var i=0; i<dataSource.length;i++){
            paid+=dataSource[i].amount;
        }
    }
    
      const columns = [
        
        {
          title: 'Fecha',
          dataIndex: 'paidDate',
          key: 'paidDate',
          
        },
        {
          title: 'Concepto',
          dataIndex: 'concept',
          key: 'concept',
          
        },
        {
            title: 'Cantidad',
            dataIndex: 'amountStr',
            key: 'amountStr',
            
          },
      ];



    return (  
        <div>
        <h3>Estatus:</h3>
            <Card>
                <Card.Grid hoverable={false} className="payment-user__card">
                    {   paid
                        ?<h3>Pagado: {" "}<strong>${paid}.00</strong> </h3>
                        :null
                    }
                </Card.Grid>
                <Card.Grid hoverable={false} className="payment-user__card">
                    {   paid&&total
                        ?<h3>Restante: {" "}<strong>${total-paid}.00</strong> </h3>
                        :null
                    }
                </Card.Grid>

            </Card>
            <h3>Recibos</h3>
            {
                dataSource
                ? <Table style={{marginLeft:"13px"}} dataSource={dataSource} columns={columns} scroll={{  y: heigh , x: width}}  />
                : <Spin tip="Cargando"/>
            }
            </div>
    );
}
 
export default PaymentInfo;