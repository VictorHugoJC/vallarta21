import React, { useEffect, useState } from 'react'
import firebase from '../../config/FireBase';
import {Spin, Avatar,Card, Descriptions  } from 'antd';
import './UserInfo.scss';
import {CheckOutlined, CloseOutlined} from '@ant-design/icons';

const UserInfo = ({id}) => {
    
    const [userData, setUserData] = useState(null);
    const [roomData, setRoomData] = useState([]);
    const [roomUsers, setRoomUsers] = useState([]);
    const [updated, setUpdated] = useState(false);

    useEffect(()=>{

        firebase.firestore().collection('usuarios').doc(id).get().then(function(doc){
            if(doc.exists){
                    setUserData(doc.data());
            }
        }).then(()=>{
            firebase.firestore().collection('habitaciones').where('users',"array-contains",id).onSnapshot(function(snapshot){
                const room= snapshot.docs.map(doc=>{
                    const roomInfo = doc.data();
                    roomInfo.id = doc.id;
                    return roomInfo
                });

                setRoomData(room);
            })
        })
    },[id]);

   
    if(roomData[0]&&updated===false){
        for(var j = 0 ; j< roomData[0].users.length; j++){
                
                firebase.firestore().collection('usuarios').doc(roomData[0].users[j]).get().then(doc=>{
                    setRoomUsers(oldArray => [...oldArray, doc.data().name]);
                  
                })
                
        }
        setUpdated(true);
    }


    return (
        <div className="user-info">
            {!userData
                ?   <div style={{justifyContent:"center", textAlign:"center", paddingTop:"20px"}}>
                        <Spin tip="cargando"    size="large"/>
                    </div>
              
                :   <div>
                        
                        <AvatarImg/>
                        <h1>{userData.name}</h1>
                        <UserData 
                            userData={userData}
                        
                        />


                    </div>
            }
            { roomData.length>0
                ? (
                    <>
                    <Room   
                        id={roomData[0].id} 
                        roomUser1 ={roomUsers[0]}
                        roomUser2 ={roomUsers[1]}
                        roomUser3 ={roomUsers[2]}
                    />
                    </>
                )
                : null

            }
        </div>
    );
}
 
export default UserInfo;

function AvatarImg(){
    return (
        <Avatar size={100} src={require('../../assets/img/avatar-default.jpg')} />
    );
}


function UserData({userData}){
    console.log();
    return (
        <>
            <Card>
                <Card.Grid style={{width:"100%"}} hoverable={false}>
                    <h4>Fecha de Nacimiento: <strong>{userData.birth}</strong></h4>
                </Card.Grid>
                <Card.Grid style={{width:"100%"}} hoverable={false}>
                    <h4>Correo Electrónico: <strong>{userData.email}</strong></h4>
                </Card.Grid>
                <Card.Grid style={{width:"100%"}} hoverable={false}>
                   {userData  ?<PaymentInfo userData={userData}/> :null}
                </Card.Grid>
            </Card>

        </>
    );
}

function PaymentInfo({userData}){
        return(
            <Descriptions title="Detalles del viaje">
           
           <Descriptions.Item >
                    {userData.boardTrip
                        ?<h4><strong><CheckOutlined/> Con paseo en barco </strong></h4>
                        :<h4><strong><CloseOutlined/> Sin Paseo en barco</strong></h4>
                    }
                    
           </Descriptions.Item>

            <Descriptions.Item >
                {
                    userData.party>0
                        ?(userData.party===1
                            ?<h4>Antros: <strong>{`${userData.party} noche`}</strong></h4>
                            :<h4>Antros: <strong>{`${userData.party} noches`}</strong></h4>

                        )
                        :<h4><strong><CloseOutlined/> Sin Antros</strong></h4>
                }
            </Descriptions.Item>
            <Descriptions.Item >
                {
                    userData.party>0
                        ?(userData.drinks>0
                            ?<h4>Barras Libres: <strong>{`${userData.drinks}`}</strong></h4>
                            :<h4><strong><CloseOutlined/> Sin Barra libre</strong></h4>)
                        :<h4> </h4>
                }
            </Descriptions.Item>
                
               
            <Descriptions.Item ><h4>Habitación: <strong>{userData.room}</strong></h4></Descriptions.Item>
            <Descriptions.Item >{userData.descount 
                ?<h4>Descuento: <strong>{`$${userData.descount}.00`}</strong></h4>
                :<h4> </h4>
            }</Descriptions.Item>
            <Descriptions.Item ><h4>Total: <strong>{`$${userData.price}.00`}</strong></h4></Descriptions.Item>
                        
          </Descriptions>
        );
}

function Room({id, roomUser1, roomUser2, roomUser3}){
    return(
        <Card>
            <Descriptions title={`código:  ${id}`}>
            

            <Descriptions.Item>
                {roomUser1
                
                    ?<h4>Roomie 1: <strong>{roomUser1}</strong></h4>
                    :<h4> </h4>
                }
            </Descriptions.Item>
                
            <Descriptions.Item>
                {roomUser2
                
                    ?<h4>Roomie 2: <strong>{roomUser2}</strong></h4>
                    :<h4> </h4>
                }
            </Descriptions.Item>
                    
            <Descriptions.Item>
                {roomUser3
                
                    ?<h4>Roomie 3: <strong>{roomUser3}</strong></h4>
                    :<h4> </h4>
                }
            </Descriptions.Item>
                            
            </Descriptions>
          </Card>
    );
}