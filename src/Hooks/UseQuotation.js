import {useContext} from 'react';
import {QuotationContext} from '../Providers/QuotationProvider';


export default () => useContext(QuotationContext);