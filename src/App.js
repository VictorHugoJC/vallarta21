import React, { Fragment } from 'react';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import routes from './config/Routes';//Arreglo que recibe los datos de rutas
import QuotationProvider from './Providers/QuotationProvider';
import AuthProvider from './Providers/AuthProvider';


function App() {
 
  return (

    <Fragment>
    <AuthProvider> 
      <QuotationProvider>
        <Router>
          <Switch>
            {routes.map((route, index) => (
              <RouteWithSubRoutes key={index} {...route} />
            ))}
          </Switch>
        </Router>
      </QuotationProvider>
    </AuthProvider>
    </Fragment>
  );
}

export default App;


function RouteWithSubRoutes(route) {
  
  return (
    <Route
      path={route.path}
      exact={route.exact}
      render={(props) => <route.component routes={route.routes} {...props} />}
    />
  );
}
